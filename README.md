# xxl-job-executor-samples

#### 介绍
系统集成 SpringBoot + xxl-job + SpringBatch +Datax
整合分布式调度系统xxl-job，调度springbatch  datax完成数据批处理。

#### 软件架构


## 平台简介
> 项目代码、文档 均开源免费可商用 遵循开源协议在项目中保留开源协议文件即可<br>
活到老写到老 为兴趣而开源 为学习而开源 为让大家真正可以学到技术而开源

| 功能介绍         | 使用技术                     | 文档地址                                                                                               | 特性注意事项                            |
|--------------|--------------------------|----------------------------------------------------------------------------------------------------|-----------------------------------|
| 后端开发框架        | SpringBoot               | [SpringBoot官网](https://spring.io/projects/spring-boot/#learn)                                      | 2.7.6版本                                 |
| 批处理开发框架      | SpringBatch               | [SpringBatch中文文档](https://www.docs4dev.com/docs/zh/spring-batch/4.1.x/reference/)       | 4.X版本                                 |
| 关系数据库         | MySQL                    | [MySQL官网](https://dev.mysql.com/)                                                                  | 适配 8.X 最低 5.7                     |
| 数据库框架         | Mybatis-Plus             | [Mybatis-Plus文档](https://baomidou.com/guide/)                                                      | 快速 CRUD 增加开发效率                    |
| 校验框架          | Validation               | [Validation文档](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/)     | 增强接口安全性、严谨性 支持国际化                 |
| 异构数据源同步框架  | Datax                   | [Datax官网](https://github.com/alibaba/DataX)     | DataX 是阿里云 DataWorks数据集成 的开源版本，在阿里巴巴集团内被广泛使用的离线数据同步工具/平台。DataX 实现了包括 MySQL、Oracle、OceanBase、SqlServer、Postgre、HDFS、Hive、ADS、HBase、TableStore(OTS)、MaxCompute(ODPS)、Hologres、DRDS, databend 等各种异构数据源之间高效的数据同步功能。                |



#### 安装教程

1.  git clone
2.  启动application


#### 使用说明

1.启动分布式调度系统xxl-job  
2.启动SpringBatch执行器
![img.png](doc/img.png)
3.启动Datax执行器  
![img_1.png](doc/img_1.png)
## datax-executor预览
### datax任务配置
![img_2.png](doc/img_2.png)
### 构建datax任务  
构建reader
![img_3.png](doc/img_3.png)
构建writer
![img_4.png](doc/img_4.png)
字段映射
![img_5.png](doc/img_5.png)
构建生成json任务
![img_6.png](doc/img_6.png)
### 数据源管理
![img_7.png](doc/img_7.png)
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

