package com.amc.common.core.exception;

/**
 * 演示模式异常
 * 
 * @author ZhangXX
 */
public class DemoModeException extends RuntimeException
{
    private static final long serialVersionUID = 1L;

    public DemoModeException()
    {
    }
}
