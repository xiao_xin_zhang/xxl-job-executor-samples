package com.amc.executor.test;

import com.amc.executor.domain.BlogInfo;
import com.amc.executor.mapper.BlogMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SpringBootTest
public class mybatisTest {
    @Autowired
    BlogMapper blogMapper;

    @Test
    public void test() {
        Map<String, Integer> map = new HashMap<>();
        map.put("authorId",1);
        List<BlogInfo> blogInfos = blogMapper.queryInfoById(map);
        System.out.println(blogInfos);
    }

}
