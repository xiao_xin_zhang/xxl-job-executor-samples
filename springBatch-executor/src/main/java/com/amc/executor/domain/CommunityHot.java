package com.amc.executor.domain;

import lombok.Data;

/**
 * 社群关注热点
 * @author ZhangXX
 * @date @2022/12/16 16:11
 */
@Data
public class CommunityHot {
    private String json_data;
}
