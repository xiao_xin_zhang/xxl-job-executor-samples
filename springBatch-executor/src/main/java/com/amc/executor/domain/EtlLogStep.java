package com.amc.executor.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value = "etl_log_step")
public class EtlLogStep {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    @TableField(value = "job_id")
    private String jobId;

    @TableField(value = "job_name")
    private String jobName;

    @TableField(value = "step_name")
    private String stepName;

    @TableField(value = "status")
    private String status;

    @TableField(value = "begin_time")
    private Date beginTime;

    @TableField(value = "end_time")
    private Date endTime;

    @TableField(value = "error_message")
    private String errorMessage;

}