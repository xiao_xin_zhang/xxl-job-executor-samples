package com.amc.executor.domain;

import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Date;

@Data
@Accessors(chain = true)
public class MonitorField {
    private String FIELD_VALUE;
    private String FIELD_ID;
    private Date FIELD_TIME;
}
