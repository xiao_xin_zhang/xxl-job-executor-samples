package com.amc.executor.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@TableName(value = "etl_log_job")
public class EtlLogJob {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * job名称
     */
    @TableField(value = "job_name")
    private String jobName;

    /**EtlLogStepMapper
     * 执行状态1：开始同步2：同步成功3：同步失败
     */
    @TableField(value = "status")
    private String status;

    /**
     * 开始时间
     */
    @TableField(value = "begin_time")
    private Date beginTime;

    /**
     * 结束时间
     */
    @TableField(value = "end_time")
    private Date endTime;

    /**
     * 运行参数
     */
    @TableField(value = "params")
    private String params;
}