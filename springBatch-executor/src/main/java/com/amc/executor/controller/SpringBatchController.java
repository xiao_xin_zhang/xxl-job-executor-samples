package com.amc.executor.controller;

import com.amc.executor.service.IEmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.batch.core.*;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;

@Api(tags = "SpringBatch")
@RestController
public class SpringBatchController {
    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job myJob;

    @ApiOperation(value = "testJob")
    @GetMapping("testJob")
    public void testJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {
        //    后置参数：使用JobParameters中绑定参数 addLong  addString 等方法
        JobParameters jobParameters = new JobParametersBuilder()
                .addLong("timeNew", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(myJob, jobParameters);
    }

    @Autowired
    Job myJobNew;

    @ApiOperation(value = "testJobNew")
    @GetMapping("testJobNew")
    public void testJobNew(@RequestParam("authorId") String authorId) throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        JobParameters jobParametersNew = new JobParametersBuilder()
                .addLong("timeNew", System.currentTimeMillis())
                .addString("authorId", authorId)
                .toJobParameters();
        jobLauncher.run(myJobNew, jobParametersNew);

    }

    @Autowired
    Job monitorDayTaskJob;

    @ApiOperation(value = "monitorDayTaskJob")
    @GetMapping("monitorDayTaskJob")
    public void monitorDayTaskJob() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {

        JobParameters jobParametersNew = new JobParametersBuilder()
                .addLong("timeNew", System.currentTimeMillis())
                .toJobParameters();
        jobLauncher.run(monitorDayTaskJob, jobParametersNew);

    }


    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    @Qualifier("csvToDBJob")  //将spring容器中名为： csvToDBJob job对象注入当前变量中
    private Job csvToDBJob;
    @Autowired
    @Qualifier("dbToDBJob")  //将spring容器中名为： csvToDBJob job对象注入当前变量中
    private Job dbToDBJob;


    @Autowired
    private JobExplorer jobExplorer;

    @ApiOperation(value = "初始化csv数据")
    @GetMapping("/dataInit")
    public String dataInit() throws IOException {
        employeeService.dataInit();
        return "ok";
    }

    @ApiOperation(value = "csvToDB")
    @GetMapping("/csvToDB")
    public String csvToDB() throws Exception {
        //允许多次执行每次操作前，将employee_temp数据清空
        employeeService.truncateTemp();
        //启动作业--可以重复执行，记录操作时间戳
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime())
                .getNextJobParameters(csvToDBJob).toJobParameters();
        JobExecution run = jobLauncher.run(csvToDBJob, parameters);
        return run.getId().toString();
    }

    @ApiOperation(value = "分区dbToDB")
    @GetMapping("/dbToDB")
    public String dbToDB() throws Exception {
        //允许多次执行每次操作前，
        employeeService.truncateAll();
        //启动作业--可以重复执行，记录操作时间戳
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime())
                .getNextJobParameters(dbToDBJob).toJobParameters();
        JobExecution run = jobLauncher.run(dbToDBJob, parameters);
        return run.getId().toString();
    }

    @Resource(name = "partitionJob")
    private Job partitionJob;

    @ApiOperation(value = "分区demo")
    @GetMapping("/demo/partitionJob")
    public String partitionJob() throws Exception {
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime()).toJobParameters();
        JobExecution run = jobLauncher.run(partitionJob, parameters);
        return run.getId().toString();
    }

    @Autowired
    @Qualifier("dbToDBPartitionJob")
    private Job dbToDBPartitionJob;

    @ApiOperation(value = "dbToDBPartitionJob")
    @GetMapping("/demo/dbToDBPartitionJob")
    public String dbToDBPartitionJob() throws Exception {
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime()).toJobParameters();
        JobExecution run = jobLauncher.run(dbToDBPartitionJob, parameters);
        return run.getId().toString();
    }

    @Resource(name = "userPartitionJob")
    private Job userPartitionJob;

    @ApiOperation(value = "userPartitionJob")
    @GetMapping("/demo/userPartitionJob")
    public String userPartitionJob() throws Exception {
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime()).toJobParameters();
        JobExecution run = jobLauncher.run(userPartitionJob, parameters);
        return run.getId().toString();
    }


    @Resource(name = "userPartitionXmlJob")
    private Job userPartitionXmlJob;

    @ApiOperation(value = "userPartitionXmlJob")
    @GetMapping("/demo/userPartitionXmlJob")
    public String userPartitionXmlJob() throws Exception {
        JobParameters parameters = new JobParametersBuilder(new JobParameters(), jobExplorer)
                .addLong("time", new Date().getTime()).toJobParameters();
        JobExecution run = jobLauncher.run(userPartitionXmlJob, parameters);
        return run.getId().toString();
    }


}
