package com.amc.executor.mapper;

import com.amc.executor.domain.EtlLogStep;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface EtlLogStepMapper extends BaseMapper<EtlLogStep> {
}