package com.amc.executor.mapper;

import com.amc.executor.domain.EtlLogJob;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface EtlLogJobMapper extends BaseMapper<EtlLogJob> {
}