package com.amc.executor.mapper;


import com.amc.executor.domain.Employee;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface EmployeeMapper extends BaseMapper<Employee> {

    /**
     * 添加
     */
    int saveEmployee(Employee employee);
    /**
     * 清空employee数据
     */
    void truncateAll();

    /**
     * 清空employee_temp数据
     */
    void truncateTemp();
}
