package com.amc.executor.batch.reader;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.amc.executor.domain.CommunityHot;
import com.amc.executor.domain.MonitorField;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 社群热点
 *
 * @author ZhangXX
 * @date 2022-12-16
 */
@Component
public class SQ006Reader implements ItemReader<MonitorField>, InitializingBean {
    private static Logger logger = LogManager.getLogger(SQ006Reader.class);

    private boolean flag;

    private Map<String, Object> jsonMap = new HashMap<>();

    @Override
    public MonitorField read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        if (flag) {
            String jsonData = getJsonData();
            if (StringUtils.isEmpty(jsonData)) {
                return null;
            }
            flag = false;
            jsonMap.put("FIELD_VALUE", jsonData);
            jsonMap.put("FIELD_ID", "SQ006");
            jsonMap.put("FIELD_TIME", new Date(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MIN).toInstant(ZoneOffset.of("+8")).toEpochMilli()));
            MonitorField monitorField = new MonitorField();
            monitorField.setFIELD_VALUE(jsonData)
                    .setFIELD_ID("SQ006")
                    .setFIELD_TIME(new Date(LocalDateTime.of(LocalDate.now().minusDays(1), LocalTime.MIN).toInstant(ZoneOffset.of("+8")).toEpochMilli()));

            return monitorField;
            //return jsonMap;
        }
        return null;
    }

    private String getJsonData() {
        logger.info("请求社群热点关注接口");
        try {
            // mock
            CommunityHot communityHot = new CommunityHot();
            communityHot.setJson_data("{\"疫情\": 0.004735298079845891, \"定投\": 0.00411427704775179, \"大盘\": 0.003723864706839685, \"医药\": 0.0034292339362353816, \"银行\": 0.002966203019634412, \"业绩\": 0.002630896046918619, \"军工\": 0.0024536404783133824, \"光伏\": 0.0024031488648531543, \"利好\": 0.0023719415548108577, \"加仓\": 0.002236233158869082, \"疫苗\": 0.0022127337452202004, \"房地产\": 0.002170041793625773, \"养老\": 0.0020772755115585234, \"感染\": 0.0020557408002915044, \"地产\": 0.0020538900398824104, \"赎回\": 0.0016876194009962497, \"etf\": 0.0016126228311741876, \"散户\": 0.0014737927671403667, \"安阳\": 0.0013749155744344865, \"半导体\": 0.0013074587131106476, \"美国\": 0.001299757082662636, \"核酸\": 0.0012552526669073142, \"恒生\": 0.001234181730542782, \"份额\": 0.0012055815717386723, \"医院\": 0.0012023268437025769, \"持仓\": 0.0011692950772250986, \"经理\": 0.0011438059879366892, \"北京\": 0.001114685649395735, \"抗原\": 0.0010764311415959714, \"牛市\": 0.0010304785541519626, \"老百姓\": 0.0010211378837838938, \"短线\": 0.0010032998135809884, \"压力\": 0.0009747624084686496, \"新能源\": 0.000948512612478638, \"病毒\": 0.0009314314939631069, \"反弹\": 0.0009082534319064048, \"回报\": 0.0009059124883901135, \"抄底\": 0.0008780827869698396, \"恢复\": 0.0008396630968048454, \"尾盘\": 0.0008182656102900514, \"高开\": 0.00080881365453909, \"建仓\": 0.0008082393351077904, \"港股\": 0.0007929334561990817, \"食品饮料\": 0.0007671330334655622, \"芯片\": 0.0007632914154165432, \"基金公司\": 0.0007540625631883954, \"申购费\": 0.0007459513134624705, \"申购\": 0.000715913181099566, \"贵州茅台\": 0.000690653657442996, \"汽车\": 0.0006616321313135707}");
            JSONObject jsonObject = JSONObject.parseObject(communityHot.getJson_data());
            List hotList = new ArrayList();
            for (Map.Entry entry : jsonObject.entrySet()) {
                Map<Object, Object> hot = new HashMap<>();
                hot.put("keyWord", entry.getKey());
                hot.put("number", entry.getValue());
                hotList.add(hot);
            }
            return JSON.toJSONString(hotList);
        } catch (Exception e) {
            logger.error("请求社群热点关注接口接口异常", e);
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }


    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }
}
