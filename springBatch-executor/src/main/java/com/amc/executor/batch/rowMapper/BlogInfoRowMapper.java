package com.amc.executor.batch.rowMapper;

import com.amc.executor.domain.BlogInfo;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 用于自定义行转换
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
public class BlogInfoRowMapper implements RowMapper<BlogInfo> {

    @Override
    public BlogInfo mapRow(ResultSet rs, int rowNum) throws SQLException {
        RowMapper<BlogInfo> rm = new BeanPropertyRowMapper<>(BlogInfo.class);
        BlogInfo blogInfo = rm.mapRow(rs, rowNum);
        return blogInfo;
    }

}
