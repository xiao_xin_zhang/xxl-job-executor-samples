package com.amc.executor.batch.rowMapper;

import com.amc.executor.domain.BlogInfo;
import com.amc.executor.domain.Employee;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 用于自定义行转换
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
public class EmployeeRowMapper implements RowMapper<Employee>{

	@Override
	public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
		RowMapper<Employee> rm = new BeanPropertyRowMapper<>(Employee.class);
		Employee employee = rm.mapRow(rs, rowNum);
		return employee;
	}

}
