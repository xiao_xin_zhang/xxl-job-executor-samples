package com.amc.executor.batch.tasklet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 用于自定义行任务
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
@Component
@Scope(value = "step")
public class DemoTasklet implements Tasklet {
    private static Logger logger = LogManager.getLogger(DemoTasklet.class);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        logger.info("处理业务");
        return RepeatStatus.FINISHED;
    }
}
