package com.amc.executor.batch.reader;

import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.batch.item.adapter.ItemReaderAdapter;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

/**
 * 用于同步完数据删除
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
public class FinalReader extends ItemReaderAdapter<Object> implements InitializingBean {

    private int limit = 1;
    private int counter = 0;

    public Object read() throws Exception, UnexpectedInputException, ParseException {
        if (counter < limit) {
            counter++;
            return new Object();
        }
        return null;
    }

    /**
     * @param limit number of items that will be generated
     *              (null returned on consecutive calls).
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getCounter() {
        return counter;
    }

    public int getLimit() {
        return limit;
    }

    public void resetCounter() {
        this.counter = 0;
    }

    public void afterPropertiesSet() throws Exception {
        Assert.notNull(limit, "limit must be set");
    }
}
