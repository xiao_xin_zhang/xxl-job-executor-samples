package com.amc.executor.batch.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component("demoPartitionStepListener")
public class DemoPartitionStepListener<I, O> implements StepExecutionListener {
	private static Logger logger = LogManager.getLogger(DemoPartitionStepListener.class);

	public void beforeStep(StepExecution stepExecution) {
		//begin--作业上下文数据共享
		long begin = System.currentTimeMillis();
		stepExecution.getExecutionContext().putLong("begin", begin);
		String modVal = (String)stepExecution.getExecutionContext().get("modVal");
		logger.info("-------------------------【DemoPartition modVal:{}开始时间：】---->"+begin+"<-----------------------------",modVal);
	}

	public ExitStatus afterStep(StepExecution stepExecution) {
		//end
		long end = System.currentTimeMillis();
		long begin = stepExecution.getExecutionContext().getLong("begin");
		String modVal = (String)stepExecution.getExecutionContext().get("modVal");

		//总消耗时间： end - begin
		logger.info("-------------------------【DemoPartition modVal:{}结束时间：】---->"+end+"<-----------------------------",modVal);
		logger.info("-------------------------【DemoPartition modVal:{}总耗时：】---->"+(end - begin)+"<-----------------------------",modVal);
		return null;
	}

}
