package com.amc.executor.batch.partitioner;

import java.util.HashMap;
import java.util.Map;

import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;


/**
 * 查询分区 根据id对3取模
 * 0  1  2 三个分区
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
public class DemoPartitioner implements Partitioner {

    @Override
    public Map<String, ExecutionContext> partition(int gridSize) {
        String text = "----Partitioner---第%s分区-----开始--------------";

        Map<String, ExecutionContext> resultMap = new HashMap<String, ExecutionContext>();
        int number = 0;
        for (int i = 0; i < gridSize; i++) {
            System.out.println(String.format(text, i, number));

            ExecutionContext context = new ExecutionContext();
            context.putInt("modVal", number);
            resultMap.put("partition" + (number++), context);
        }

        return resultMap;
    }

}
