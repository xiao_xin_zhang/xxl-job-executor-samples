package com.amc.executor.batch.listener;

import com.amc.executor.domain.BlogInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;

import java.util.List;

public class MyWriteListener implements ItemWriteListener<BlogInfo> {
    private Logger logger = LoggerFactory.getLogger(MyWriteListener.class);

    @Override
    public void beforeWrite(List<? extends BlogInfo> items) {
    }

    @Override
    public void afterWrite(List<? extends BlogInfo> items) {
    }

    @Override
    public void onWriteError(Exception exception, List<? extends BlogInfo> items) {
        try {
            logger.info(exception.getMessage());
            for (BlogInfo message : items) {
                logger.info("Failed writing BlogInfo : {}", message.toString());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}