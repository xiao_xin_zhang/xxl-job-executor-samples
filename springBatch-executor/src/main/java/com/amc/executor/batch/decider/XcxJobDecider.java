package com.amc.executor.batch.decider;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.job.flow.FlowExecutionStatus;
import org.springframework.batch.core.job.flow.JobExecutionDecider;
import org.springframework.stereotype.Component;

/**
 * 程序化流程决策
 *
 * @author ZhangXX
 * 2023-01-09 14:44
 */
@Component
public class XcxJobDecider implements JobExecutionDecider {
    private static Logger logger = LogManager.getLogger(XcxJobDecider.class);

    @Override
    public FlowExecutionStatus decide(JobExecution jobExecution, StepExecution stepExecution) {
        int count = 0;
        if (count == 0) {
            return FlowExecutionStatus.FAILED;
        } else {
            return FlowExecutionStatus.COMPLETED;
        }
    }
}
