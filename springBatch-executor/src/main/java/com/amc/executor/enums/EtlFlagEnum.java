package com.amc.executor.enums;

/**
 * 任务状态
 *
 * @author ZhangXX
 */
public enum EtlFlagEnum {
    UNKNOW("不满足启动条件"),
    START("开始"),
    SUCCESS("成功"),
    FAILED("失败");

    private String desc;

    private EtlFlagEnum(String desc) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
