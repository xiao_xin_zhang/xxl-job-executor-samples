package com.amc.executor.enums;

/**
 * 同步状态
 *
 * @author ZhangXX
 */
public enum EtlStatusEnum {
    STATUS0("0", "未执行"),
    STATUS1("1", "执行中"),
    STATUS2("2", "成功"),
    STATUS3("3", "失败");

    private String code;
    private String desc;

    private EtlStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
