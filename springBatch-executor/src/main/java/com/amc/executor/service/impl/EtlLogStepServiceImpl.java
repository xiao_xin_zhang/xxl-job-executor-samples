package com.amc.executor.service.impl;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.amc.executor.domain.EtlLogStep;
import com.amc.executor.mapper.EtlLogStepMapper;
import com.amc.executor.service.EtlLogStepService;
@Service
public class EtlLogStepServiceImpl extends ServiceImpl<EtlLogStepMapper, EtlLogStep> implements EtlLogStepService{

}
