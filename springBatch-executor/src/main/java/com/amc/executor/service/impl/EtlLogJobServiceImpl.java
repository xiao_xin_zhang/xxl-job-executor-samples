package com.amc.executor.service.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.amc.executor.domain.EtlLogJob;
import com.amc.executor.mapper.EtlLogJobMapper;
import com.amc.executor.service.EtlLogJobService;
@Service
public class EtlLogJobServiceImpl extends ServiceImpl<EtlLogJobMapper, EtlLogJob> implements EtlLogJobService{

}
