package com.amc.executor;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.scope.StepScope;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.ImportResource;


/**
 * @author ZhangXX
 * 2023-01-03 00:38:13
 */
// 表示通过aop框架暴露该代理对象,AopContext能够访问
@EnableBatchProcessing
//@EnableAspectJAutoProxy(exposeProxy = true)
@SpringBootApplication
@ImportResource(locations = {"classpath:/job/**/*.xml"})
public class SpringBatchExecutorApplication {
    @Bean
    public StepScope stepScope() {
        return new StepScope();
    }
    public static void main(String[] args) {
        SpringApplication.run(SpringBatchExecutorApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  SpringBatch系统启动成功   ლ(´ڡ`ლ)ﾞ )");
    }

}