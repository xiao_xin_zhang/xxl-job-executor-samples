package com.amc.executor.controller.datax;

import com.amc.common.core.web.controller.BaseController;
import com.amc.common.core.web.domain.AjaxResult;
import com.amc.executor.dto.DataXJsonBuildDto;
import com.amc.executor.service.DataxJsonService;
import com.amc.executor.util.JSONUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author ZhangXX
 * 2022/05/14
 */
@RestController
@RequestMapping("api/dataxJson")
@Api(tags = "组装datax  json的控制器")
public class DataxJsonController extends BaseController {

    @Autowired
    private DataxJsonService dataxJsonService;

    @PostMapping("/buildJson")
    @ApiOperation("JSON构建")
    public AjaxResult buildJobJson(@RequestBody DataXJsonBuildDto dto) {
        if (dto.getReaderDatasourceId() == null) {
            return AjaxResult.error("请选择Reader数据源");
        }
        if (dto.getWriterDatasourceId() == null) {
            return AjaxResult.error("请选择Writer数据源");
        }
/*        if (CollectionUtils.isEmpty(dto.getReaderColumns())) {
            return AjaxResult.error("请选择Reader Column");
        }
        if (CollectionUtils.isEmpty(dto.getWriterColumns())) {
            return AjaxResult.error("请选择Writer Column");
        }*/
        return AjaxResult.success("构建成功", dataxJsonService.buildJobJson(dto));
    }

    /**
     * 格式化Json
     *
     * @return
     */
    @ApiOperation("格式化Json")
    @PostMapping("/format")
    @ResponseBody
    public AjaxResult formatJson(@RequestBody String strJson) {
        return AjaxResult.success("格式化成功", JSONUtils.formatJson(strJson));
    }

}
