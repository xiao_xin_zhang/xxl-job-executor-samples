package com.amc.executor.controller.datax;

import com.amc.common.core.web.controller.BaseController;
import com.amc.common.core.web.domain.AjaxResult;
import com.amc.executor.service.DatasourceQueryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.SQLException;

/**
 * 查询数据库表名，字段的控制器
 *
 * @author ZhangXX
 * 2022/05/14
 */
@RestController
@RequestMapping("api/metadata")
@Api(tags = "jdbc数据库查询控制器")
public class MetadataController extends BaseController {

    @Autowired
    private DatasourceQueryService datasourceQueryService;

    /**
     * 根据数据源id获取mongo库名
     *
     * @param datasourceId
     * @return
     */
    @GetMapping("/getDBs")
    @ApiOperation("根据数据源id获取mongo库名")
    public AjaxResult getDBs(Long datasourceId) throws IOException {
        return AjaxResult.success(datasourceQueryService.getDBs(datasourceId));
    }


    /**
     * 根据数据源id,dbname获取CollectionNames
     *
     * @param datasourceId
     * @return
     */
    @GetMapping("/collectionNames")
    @ApiOperation("根据数据源id,dbname获取CollectionNames")
    public AjaxResult getCollectionNames(Long datasourceId, String dbName) throws IOException {
        return AjaxResult.success(datasourceQueryService.getCollectionNames(datasourceId, dbName));
    }

    /**
     * 获取PG table schema
     *
     * @param datasourceId
     * @return
     */
    @GetMapping("/getDBSchema")
    @ApiOperation("根据数据源id获取 db schema")
    public AjaxResult getTableSchema(Long datasourceId) {
        return AjaxResult.success(datasourceQueryService.getTableSchema(datasourceId));
    }

    /**
     * 根据数据源id获取可用表名
     *
     * @param datasourceId
     * @return
     */
    @GetMapping("/getTables")
    @ApiOperation("根据数据源id获取可用表名")
    public AjaxResult getTableNames(Long datasourceId, String tableSchema) throws IOException {
        return AjaxResult.success(datasourceQueryService.getTables(datasourceId, tableSchema));
    }

    /**
     * 根据数据源id和表名获取所有字段
     *
     * @param datasourceId 数据源id
     * @param tableName    表名
     * @return
     */
    @GetMapping("/getColumns")
    @ApiOperation("根据数据源id和表名获取所有字段")
    public AjaxResult getColumns(Long datasourceId, String tableName) throws IOException {
        return AjaxResult.success(datasourceQueryService.getColumns(datasourceId, tableName));
    }

    /**
     * 根据数据源id和sql语句获取所有字段
     *
     * @param datasourceId 数据源id
     * @param querySql     表名
     * @return
     */
    @GetMapping("/getColumnsByQuerySql")
    @ApiOperation("根据数据源id和sql语句获取所有字段")
    public AjaxResult getColumnsByQuerySql(Long datasourceId, String querySql) throws SQLException {
        return AjaxResult.success(datasourceQueryService.getColumnsByQuerySql(datasourceId, querySql));
    }
}
