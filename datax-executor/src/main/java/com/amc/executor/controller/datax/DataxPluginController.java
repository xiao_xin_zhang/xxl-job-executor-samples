package com.amc.executor.controller.datax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * dataxPlugin controller
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Controller
@RequestMapping("/datax")
public class DataxPluginController {
    private static Logger logger = LoggerFactory.getLogger(DataxPluginController.class);

    private String prefix = "/datax";

    /**
     * reader组件刷新
     */
    @PostMapping("/reader/localrefresh")
    public String readerRefresh(String fragment) {
        return prefix + "/reader/" + fragment + "::" + fragment;
    }

    /**
     * writer组件刷新
     */
    @PostMapping("/writer/localrefresh")
    public String writerRefresh(String fragment) {
        return prefix + "/writer/" + fragment + "::" + fragment;
    }
}
