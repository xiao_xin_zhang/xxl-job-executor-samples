package com.amc.executor.controller.datax;

import com.amc.common.core.text.Convert;
import com.amc.common.core.web.controller.BaseController;
import com.amc.common.core.web.domain.AjaxResult;
import com.amc.common.core.web.page.TableDataInfo;
import com.amc.executor.constant.UserConstants;
import com.amc.executor.domain.JobDatasource;
import com.amc.executor.domain.SysDictType;
import com.amc.executor.service.JobDatasourceService;
import com.amc.executor.util.LocalCacheUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * jdbc数据源配置控制器层
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Controller
@RequestMapping("/datax/datasource")
@Api(tags = "jdbc数据源配置接口")
public class JobDatasourceController extends BaseController {
    private String prefix = "datax/datasource";
    /**
     * 服务对象
     */
    @Autowired
    private JobDatasourceService jobJdbcDatasourceService;

    @GetMapping()
    public String jobDatasource() {
        return prefix + "/datasource";
    }

    /**
     * 分页查询所有数据
     *
     * @return 所有数据
     */
    @ApiOperation("分页查询所有数据")
    @PostMapping(value = "/list")
    @ResponseBody
    public TableDataInfo list(JobDatasource jobDatasource) {
        startPage();
        List<JobDatasource> datasources = jobJdbcDatasourceService.selectJobDatasourceList(jobDatasource);
        return getDataTable(datasources);
    }


    /**
     * 获取所有数据源
     *
     * @return
     */
    @ApiOperation("获取所有数据源")
    @GetMapping("/all")
    @ResponseBody
    public AjaxResult selectAllDatasource() {
        return AjaxResult.success(this.jobJdbcDatasourceService.selectAllDatasource());
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation("通过主键查询单条数据")
    @GetMapping("{id}")
    @ResponseBody
    public AjaxResult selectOne(@PathVariable Serializable id) {
        return AjaxResult.success(this.jobJdbcDatasourceService.getById(id));
    }

    /**
     * 新增新增数据源
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }


    /**
     * 新增数据源
     *
     * @param entity 实体对象
     * @return 新增结果
     */
    @ApiOperation("新增数据源")
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult insert(JobDatasource entity) {
        return AjaxResult.success(this.jobJdbcDatasourceService.save(entity));
    }

    /**
     * 修改字典类型
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        mmap.put("jobJdbcDatasource", jobJdbcDatasourceService.getById(id));
        return prefix + "/edit";
    }

    /**
     * 修改数据源
     */
    @ApiOperation("修改数据源")
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult update(JobDatasource entity) {
        LocalCacheUtil.remove(entity.getDatasourceName());
        JobDatasource d = jobJdbcDatasourceService.getById(entity.getId());
        if (null != d.getUsername() && entity.getUsername().equals(d.getUsername())) {
            entity.setUsername(null);
        }
        if (null != entity.getPassword() && entity.getPassword().equals(d.getPassword())) {
            entity.setPassword(null);
        }
        return success(this.jobJdbcDatasourceService.updateById(entity));
    }

    /**
     * 删除数据源
     *
     * @param ids 主键结合
     * @return 删除结果
     */
    @ApiOperation("删除数据源")
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult delete(String ids) {
        return success(this.jobJdbcDatasourceService.removeByIds(Arrays.asList(Convert.toLongArray(ids))));
    }

    /**
     * 测试数据源
     *
     * @param jobDatasource
     * @return
     */
    @PostMapping("/test")
    @ApiOperation("测试数据")
    @ResponseBody
    public AjaxResult dataSourceTest(JobDatasource jobDatasource) throws IOException {
        return success(jobJdbcDatasourceService.dataSourceTest(jobDatasource));
    }
}