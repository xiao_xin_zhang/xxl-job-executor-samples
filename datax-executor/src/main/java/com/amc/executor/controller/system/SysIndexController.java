package com.amc.executor.controller.system;

import com.amc.common.core.config.GlobalConfig;
import com.amc.common.core.utils.StringUtils;
import com.amc.common.core.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;


/**
 * 首页 业务处理
 *
 * @author ZhangXX
 */
@Controller
public class SysIndexController extends BaseController {

    // 系统首页
    @GetMapping("/index")
    public String index(ModelMap mmap) {
        mmap.put("userName", "admin");
        mmap.put("footer", true);
        mmap.put("tagsView", true);
        mmap.put("mainClass", contentMainClass(true, true));
        mmap.put("copyrightYear", GlobalConfig.getCopyrightYear());
        mmap.put("isMobile", false);
        return "index";
    }

    // content-main class
    public String contentMainClass(Boolean footer, Boolean tagsView)
    {
        if (!footer && !tagsView)
        {
            return "tagsview-footer-hide";
        }
        else if (!footer)
        {
            return "footer-hide";
        }
        else if (!tagsView)
        {
            return "tagsview-hide";
        }
        return StringUtils.EMPTY;
    }
    // 切换主题
    @GetMapping("/system/switchSkin")
    public String switchSkin() {
        return "skin";
    }

    // 系统介绍
    @GetMapping("/system/main")
    public String main(ModelMap mmap) {
        mmap.put("version", GlobalConfig.getVersion());
        return "main";
    }

}
