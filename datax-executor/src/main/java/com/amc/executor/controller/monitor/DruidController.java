package com.amc.executor.controller.monitor;

import com.amc.common.core.web.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * druid 监控
 * 
 * @author ZhangXX
 */
@Controller
@RequestMapping("/monitor/data")
public class DruidController extends BaseController
{
    private String prefix = "/druid";

    @GetMapping()
    public String index()
    {
        return redirect(prefix + "/index.html");
    }
}
