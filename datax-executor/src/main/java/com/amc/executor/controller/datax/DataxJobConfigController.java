package com.amc.executor.controller.datax;

import com.amc.common.core.text.Convert;
import com.amc.common.core.web.controller.BaseController;
import com.amc.common.core.web.domain.AjaxResult;
import com.amc.common.core.web.page.TableDataInfo;
import com.amc.executor.domain.DataxJobConfig;
import com.amc.executor.service.DataxJobConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;


/**
 * datax任务配置信息Controller
 *
 * @author ZhangXX
 * @date 2023-01-16
 */
@Controller
@RequestMapping("/datax/config")
public class DataxJobConfigController extends BaseController {
    private String prefix = "datax/config";

    @Autowired
    private DataxJobConfigService dataxJobConfigService;

    @GetMapping()
    public String config() {
        return prefix + "/config";
    }

    /**
     * 查询datax任务配置信息列表
     */
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(DataxJobConfig dataxJobConfig) {
        startPage();
        List<DataxJobConfig> list = dataxJobConfigService.selectDataxJobConfigList(dataxJobConfig);
        return getDataTable(list);
    }

    /**
     * 新增datax任务配置信息
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存datax任务配置信息
     */
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(DataxJobConfig dataxJobConfig) {
        return toAjax(dataxJobConfigService.save(dataxJobConfig));
    }

    /**
     * 修改datax任务配置信息
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        DataxJobConfig dataxJobConfig = dataxJobConfigService.selectDataxJobConfigById(id);
        mmap.put("dataxJobConfig", dataxJobConfig);
        return prefix + "/edit";
    }

    /**
     * 修改保存datax任务配置信息
     */
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(DataxJobConfig dataxJobConfig) {
        return toAjax(dataxJobConfigService.updateById(dataxJobConfig));
    }

    /**
     * 删除datax任务配置信息
     */
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(dataxJobConfigService.removeByIds(Arrays.asList(Convert.toLongArray(ids))));
    }
}
