package com.amc.executor.controller.datax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * dataxConfig build controller
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Controller
@RequestMapping("/datax/build")
public class DataxBuildController {
    private static Logger logger = LoggerFactory.getLogger(DataxBuildController.class);

    private String prefix = "datax/build";

    /**
     * 表单向导
     */
    @GetMapping()
    public String build() {
        return prefix + "/build";
    }

}
