package com.amc.executor.jobhandler;


import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.biz.model.TriggerParam;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

/**
 * DataX任务终止
 *
 * @author ZhangXX
 * 2022/05/14
 */
//@JobHandler(value = "killJobHandler")
@Component
public class KillJobHandler {

    @XxlJob("killJobHandler")
    public ReturnT<String> execute(TriggerParam tgParam) {
/*        String processId = tgParam.getProcessId();
        boolean result = ProcessUtil.killProcessByPid(processId);
        //  删除临时文件
        if (!CollectionUtils.isEmpty(JobTmpFile.jobTmpFiles)) {
            String pathname = JobTmpFile.jobTmpFiles.get(processId);
            if (pathname != null) {
                FileUtil.del(new File(pathname));
                JobTmpFile.jobTmpFiles.remove(processId);
            }
        }
        return result ? ReturnT.SUCCESS : ReturnT.FAIL;*/
        return null;
    }

}
