package com.amc.executor.jobhandler;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import com.amc.executor.domain.DataxJobConfig;
import com.amc.executor.domain.JobDatasource;
import com.amc.executor.enums.IncrementTypeEnum;
import com.amc.executor.mapper.JobDatasourceMapper;
import com.amc.executor.service.DataxJobConfigService;
import com.amc.executor.service.JobDatasourceService;
import com.amc.executor.service.command.BuildCommand;
import com.amc.executor.service.logparse.AnalysisStatistics;
import com.amc.executor.service.logparse.LogStatistics;
import com.amc.executor.tool.query.BaseQueryTool;
import com.amc.executor.tool.query.QueryToolFactory;
import com.amc.executor.util.ProcessUtil;
import com.amc.executor.util.SystemUtils;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.xxl.job.core.biz.model.ReturnT;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.*;
import java.lang.invoke.LambdaConversionException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.FutureTask;

import static com.amc.executor.constant.DataXConstant.DEFAULT_JSON;

/**
 * DataX任务运行
 *
 * @author ZhangXX 2022-10-16
 */

@Component
public class ExecutorJobHandler {

    @Value("${datax.executor.jsonpath}")
    private String jsonPath;

    @Value("${datax.executor.pypath}")
    private String dataXPyPath;
    @Autowired
    private JobDatasourceService jobDatasourceService;
    @Autowired
    private DataxJobConfigService dataxJobConfigService;

    @XxlJob("executorJobHandler")
    public ReturnT<String> execute() {
        // 根据调度id获取datax任务的配置信息
        String param = XxlJobHelper.getJobParam();
        Long jobId = XxlJobHelper.getJobId();
        XxlJobHelper.log("调度任务id:{}", jobId);
        DataxJobConfig dataxJobConfig = getDataxJobConfig(jobId);
        int exitValue = -1;
        Thread errThread = null;
        String tmpFilePath;
        LogStatistics logStatistics = null;
        // Generate Datax JSON temporary file
        tmpFilePath = generateTemJsonFile(dataxJobConfig.getJobJson());

        try {
            String[] cmdarrayFinal = BuildCommand.buildDataXExecutorCmd(dataxJobConfig, tmpFilePath, dataXPyPath);
            final Process process = Runtime.getRuntime().exec(cmdarrayFinal);
            String prcsId = ProcessUtil.getProcessId(process);
            XxlJobHelper.log("------------------DataX process id: " + prcsId);
            JobTmpFile.jobTmpFiles.put(prcsId, tmpFilePath);
            //update datax process id
/*            HandleProcessCallbackParam prcs = new HandleProcessCallbackParam(trigger.getLogId(), trigger.getLogDateTime(), prcsId);
            ProcessCallbackThread.pushCallBack(prcs);*/
            // log-thread
            Thread futureThread = null;
            FutureTask<LogStatistics> futureTask = new FutureTask<>(() -> AnalysisStatistics.analysisStatisticsLog(new BufferedInputStream(process.getInputStream())));
            futureThread = new Thread(futureTask);
            futureThread.start();

            errThread = new Thread(() -> {
                try {
                    AnalysisStatistics.analysisStatisticsLog(new BufferedInputStream(process.getErrorStream()));
                } catch (IOException e) {
                    XxlJobHelper.log(e);
                }
            });

            logStatistics = futureTask.get();
            errThread.start();
            // process-wait
            // exit code: 0=success, 1=error
            exitValue = process.waitFor();
            // log-thread join
            errThread.join();
        } catch (Exception e) {
            XxlJobHelper.log(e);
        } finally {
            if (errThread != null && errThread.isAlive()) {
                errThread.interrupt();
            }
            //  删除临时文件
            if (FileUtil.exist(tmpFilePath)) {
                FileUtil.del(new File(tmpFilePath));
            }
        }
        if (exitValue == 0) {
            updateDataxJobConfig(jobId, dataxJobConfig);
            return new ReturnT<>(200, logStatistics.toString());
        } else {
            return new ReturnT<>(ReturnT.FAIL.getCode(), "command exit value(" + exitValue + ") is failed");
        }
    }


    private String generateTemJsonFile(String jobJson) {
        String tmpFilePath;
        String dataXHomePath = SystemUtils.getDataXHomePath();
        if (StringUtils.isNotEmpty(dataXHomePath)) {
            jsonPath = dataXHomePath + DEFAULT_JSON;
        }
        if (!FileUtil.exist(jsonPath)) {
            FileUtil.mkdir(jsonPath);
        }
        tmpFilePath = jsonPath + "jobTmp-" + IdUtil.simpleUUID() + ".conf";
        // 根据json写入到临时本地文件
        try (PrintWriter writer = new PrintWriter(tmpFilePath, "UTF-8")) {
            writer.println(jobJson);
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            XxlJobHelper.log("JSON 临时文件写入异常：" + e.getMessage());
        }
        return tmpFilePath;
    }

    /**
     * 根据调度id获取datax任务的配置信息
     *
     * @return DataxJobConfig
     */
    private DataxJobConfig getDataxJobConfig(Long jobId) {
        XxlJobHelper.log("根据jobid获取DataxJobConfig信息");
        LambdaQueryWrapper<DataxJobConfig> wrapper = new LambdaQueryWrapper<DataxJobConfig>()
                .eq(DataxJobConfig::getJobInfoId, jobId);
        DataxJobConfig dataxJobConfig = dataxJobConfigService.getOne(wrapper);

        if (IncrementTypeEnum.ID.getCode().equals(dataxJobConfig.getIncrementType())) {
            long maxId = getMaxId(dataxJobConfig);
            dataxJobConfig.setIncEndId(maxId);
        }
        if (IncrementTypeEnum.TIME.getCode().equals(dataxJobConfig.getIncrementType())) {
            dataxJobConfig.setIncEndTime(new Date());
        }
        return dataxJobConfig;
    }

    /**
     * 更新增量datax任务的配置信息
     *
     * @return DataxJobConfig
     */
    private void updateDataxJobConfig(Long jobId, DataxJobConfig dataxJobConfig) {
        LambdaUpdateWrapper<DataxJobConfig> wrapper = null;
        if (IncrementTypeEnum.ID.getCode().equals(dataxJobConfig.getIncrementType())) {
            wrapper = new LambdaUpdateWrapper<DataxJobConfig>()
                    .set(DataxJobConfig::getIncStartId, dataxJobConfig.getIncEndId())
                    .eq(DataxJobConfig::getJobInfoId, jobId);
            XxlJobHelper.log("根据jobid更新DataxJobConfig中的IncStartId");
        }
        if (IncrementTypeEnum.TIME.getCode().equals(dataxJobConfig.getIncrementType())) {
            wrapper = new LambdaUpdateWrapper<DataxJobConfig>()
                    .set(DataxJobConfig::getIncStartTime, dataxJobConfig.getIncEndTime())
                    .eq(DataxJobConfig::getJobInfoId, jobId);
            XxlJobHelper.log("根据jobid更新DataxJobConfig中的getIncStartTime");
        }
        if (wrapper != null) {
            boolean b = dataxJobConfigService.update(wrapper);
        }
    }

    /**
     * 获取读表的主键id最大值
     *
     * @param config
     * @return
     */
    private long getMaxId(DataxJobConfig config) {
        JobDatasource datasource = jobDatasourceService.getById(config.getDatasourceId());
        BaseQueryTool qTool = QueryToolFactory.getByDbType(datasource);
        return qTool.getMaxIdVal(config.getReaderTable(), config.getPrimaryKey());
    }

}
