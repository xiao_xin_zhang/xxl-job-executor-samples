package com.amc.executor.dto;

import lombok.Data;

@Data
public class Column {

  private Integer index;

  private String type;

  private String format;
}
