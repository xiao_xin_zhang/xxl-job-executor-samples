package com.amc.executor.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 构建mongodb write dto
 * @author ZhangXX
 * 2022/05/14
 */
@Data
public class MongoDBWriterDto implements Serializable {

    private UpsertInfo upsertInfo;

}
