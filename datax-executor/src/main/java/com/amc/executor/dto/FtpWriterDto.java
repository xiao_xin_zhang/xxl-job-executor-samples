package com.amc.executor.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class FtpWriterDto implements Serializable {

  private String writerProtocol;
  private String writerPath;
  private String writerFileName;
  private String writeMode;
  private String writerEncoding;
  private String writerNullFormat;
  private String writerDateFormat;
  private String writerFileFormat;
  private String writerSuffix;
}
