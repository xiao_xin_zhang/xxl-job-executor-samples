package com.amc.executor.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 构建json dto
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Data
public class RdbmsReaderDto implements Serializable {

    private String readerSplitPk;

    private String whereParams;

    private String querySql;
}
