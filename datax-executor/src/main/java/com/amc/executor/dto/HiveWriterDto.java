package com.amc.executor.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 构建hive write dto
 * @author ZhangXX
 * 2022/05/14
 */
@Data
public class HiveWriterDto implements Serializable {

    private String writerDefaultFS;

    private String writerFileType;

    private String writerPath;

    private String writerFileName;

    private String writeMode;

    private String writeFieldDelimiter;
}
