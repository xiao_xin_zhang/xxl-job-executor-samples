package com.amc.executor.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class FtpReaderDto implements Serializable {

  private String readerProtocol;
  private String readerPath;

  private String readerEncoding;
  private String readerNullFormat;
  private String readerColumn;
}
