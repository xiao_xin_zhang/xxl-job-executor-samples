package com.amc.executor.enums;

/**
 * 增量
 *
 * @author ZhangXX
 * 2022/05/14
 */
public enum IncrementTypeEnum {
    /**
     * 2 TIME
     * 1 ID
     * 3 PARTITION
     */
    TIME("2", "时间"),
    ID("1", "自增主键"),
    PARTITION("3", "HIVE分区");

    IncrementTypeEnum(String code, String descp){
        this.code = code;
        this.descp = descp;
    }

    private final String code;
    private final String descp;

    public String getCode() {
        return code;
    }

    public String getDescp() {
        return descp;
    }
}

