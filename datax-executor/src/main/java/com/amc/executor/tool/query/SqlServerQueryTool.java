package com.amc.executor.tool.query;

import com.amc.executor.domain.JobDatasource;

import java.sql.SQLException;

/**
 * sql server
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class SqlServerQueryTool extends BaseQueryTool implements QueryToolInterface {
    public SqlServerQueryTool(JobDatasource jobDatasource) throws SQLException {
        super(jobDatasource);
    }
}
