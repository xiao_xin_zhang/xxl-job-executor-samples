package com.amc.executor.tool.datax.writer;

import com.amc.executor.tool.datax.DataxPluginInterface;

/**
 * 用于构建writer的接口
 *
 * @author ZhangXX
 * 2022/05/14
 */
public interface DataxWriterInterface extends DataxPluginInterface {


}
