package com.amc.executor.tool.datax.reader;

import com.amc.executor.tool.datax.DataxPluginInterface;

/**
 * 用于构建reader的接口
 *
 * @author ZhangXX
 * 2022/05/14
 */
public interface DataxReaderInterface extends DataxPluginInterface {


}
