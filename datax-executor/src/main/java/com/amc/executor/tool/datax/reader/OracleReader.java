package com.amc.executor.tool.datax.reader;

import java.util.Map;

/**
 * oracle reader 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class OracleReader extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "oraclereader";
    }

    @Override
    public Map<String, Object> sample() {
        return null;
    }
}
