package com.amc.executor.tool.datax.reader;

import java.util.Map;

/**
 * sqlserver reader 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class SqlServerReader extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "sqlserverreader";
    }

    @Override
    public Map<String, Object> sample() {
        return null;
    }
}
