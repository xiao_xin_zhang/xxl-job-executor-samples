package com.amc.executor.tool.datax.reader;

import com.alibaba.fastjson.JSON;
import com.amc.executor.domain.JobDatasource;
import com.amc.executor.tool.pojo.DataxFtpPojo;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * ftp reader 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class FtpReader extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "ftpreader";
    }

    @Override
    public Map<String, Object> sample() {
        return null;
    }


    @Override
    public Map<String, Object> buildFtp(DataxFtpPojo plugin) {
        //构建
        Map<String, Object> readerObj = Maps.newLinkedHashMap();
        JobDatasource jobDatasource = plugin.getJobDatasource();
        readerObj.put("username", jobDatasource.getUsername());
        readerObj.put("password", jobDatasource.getPassword());
        readerObj.put("host", jobDatasource.getHost());
        readerObj.put("port", jobDatasource.getPort());
        Map<String, Object> parameterObj = Maps.newLinkedHashMap();
        parameterObj.put("protocol", plugin.getReaderProtocol());
        parameterObj.put("path", plugin.getReaderPath());
        parameterObj.put("column", JSON.parseArray(plugin.getReaderColumn()));
        parameterObj.put("fieldDelimiter", plugin.getReaderFieldDelimiter());
        parameterObj.put("encoding", plugin.getReaderEncoding());
        readerObj.put("parameter", parameterObj);
        return readerObj;
    }
}
