package com.amc.executor.tool.query;

import com.amc.executor.domain.JobDatasource;

import java.sql.SQLException;

/**
 * hive
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class HiveQueryTool extends BaseQueryTool implements QueryToolInterface {
    public HiveQueryTool(JobDatasource jobDatasource) throws SQLException {
        super(jobDatasource);
    }
}
