package com.amc.executor.tool.datax;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 抽象实现类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public abstract class BaseDataxPlugin implements DataxPluginInterface {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

}
