package com.amc.executor.tool.pojo;


import com.amc.executor.domain.JobDatasource;
import com.amc.executor.dto.Column;
import com.amc.executor.dto.Range;
import com.amc.executor.dto.VersionColumn;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * 用于传参，构建json
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Data
public class DataxFtpPojo {

    private List<Map<String, Object>> columns;

    /**
     * 数据源信息
     */
    private JobDatasource jobDatasource;

    private List<String> readerPath;
    private String readerColumn;
    private String readerFieldDelimiter;
    private String readerProtocol;
    private String readerEncoding;

    private String writerProtocol;
    private String writerPath;
    private String writerFileName;
    private String writerFileFormat;
    private String writerSuffix;
    private String writeMode;
    private String writeNullFormat;
    private String writeFieldDelimiter;
    private String writeDateFormat;
    private String writeEncoding;

}
