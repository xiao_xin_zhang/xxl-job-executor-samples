package com.amc.executor.tool.datax.reader;


import java.util.Map;

/**
 * mysql reader 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class MysqlReader extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "mysqlreader";
    }


    @Override
    public Map<String, Object> sample() {
        return null;
    }
}
