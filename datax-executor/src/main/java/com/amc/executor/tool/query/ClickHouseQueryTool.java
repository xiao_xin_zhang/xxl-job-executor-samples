package com.amc.executor.tool.query;

import com.amc.executor.domain.JobDatasource;

import java.sql.SQLException;

/**
 * ClickHouse
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class ClickHouseQueryTool extends BaseQueryTool implements QueryToolInterface {
    /**
     * 构造方法
     *
     * @param jobJdbcDatasource
     */
  public ClickHouseQueryTool(JobDatasource jobJdbcDatasource) throws SQLException {
        super(jobJdbcDatasource);
    }
}
