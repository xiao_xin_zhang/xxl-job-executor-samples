package com.amc.executor.tool.datax.reader;

import java.util.Map;

/**
 * postgresql 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class PostgresqlReader extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "postgresqlreader";
    }

    @Override
    public Map<String, Object> sample() {
        return null;
    }
}
