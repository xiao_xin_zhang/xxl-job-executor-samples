package com.amc.executor.tool.datax;


import com.amc.executor.tool.pojo.*;

import java.util.Map;

/**
 * 插件基础接口
 *
 * @author ZhangXX
 * 2022/05/14
 */
public interface DataxPluginInterface {
    /**
     * 获取reader插件名称
     *
     * @return
     */
    String getName();

    /**
     * 构建
     *
     * @return dataxPluginPojo
     */
    Map<String, Object> build(DataxRdbmsPojo dataxPluginPojo);


    /**
     * hive json构建
     * @param dataxHivePojo
     * @return
     */
    Map<String, Object> buildHive(DataxHivePojo dataxHivePojo);

    /**
     * hbase json构建
     * @param dataxHbasePojo
     * @return
     */
    Map<String, Object> buildHbase(DataxHbasePojo dataxHbasePojo);

    /**
     * mongodb json构建
     * @param dataxMongoDBPojo
     * @return
     */
    Map<String,Object> buildMongoDB(DataxMongoDBPojo dataxMongoDBPojo);

    Map<String, Object> buildFtp(DataxFtpPojo dataxFtpPojo);
    /**
     * 获取示例
     *
     * @return
     */
    Map<String, Object> sample();
}
