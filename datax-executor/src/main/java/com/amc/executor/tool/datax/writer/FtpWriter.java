package com.amc.executor.tool.datax.writer;

import com.amc.executor.domain.JobDatasource;
import com.amc.executor.tool.datax.reader.BaseReaderPlugin;
import com.amc.executor.tool.datax.reader.DataxReaderInterface;
import com.amc.executor.tool.pojo.DataxFtpPojo;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * ftp Writer 构建类
 *
 * @author ZhangXX
 * 2022/05/14
 */
public class FtpWriter extends BaseReaderPlugin implements DataxReaderInterface {
    @Override
    public String getName() {
        return "ftpwriter";
    }

    @Override
    public Map<String, Object> sample() {
        return null;
    }


    @Override
    public Map<String, Object> buildFtp(DataxFtpPojo plugin) {
        //构建
        Map<String, Object> writerObj = Maps.newLinkedHashMap();
        JobDatasource jobDatasource = plugin.getJobDatasource();
        writerObj.put("username", jobDatasource.getUsername());
        writerObj.put("password", jobDatasource.getPassword());
        writerObj.put("host", jobDatasource.getHost());
        writerObj.put("port", jobDatasource.getPort());
        Map<String, Object> parameterObj = Maps.newLinkedHashMap();
        parameterObj.put("protocol", plugin.getWriterProtocol());
        parameterObj.put("path", plugin.getWriterPath());
        parameterObj.put("fileName", plugin.getWriterFileName());
        parameterObj.put("fileFormat", plugin.getWriterFileFormat());
        parameterObj.put("suffix", plugin.getWriterSuffix());
        parameterObj.put("fieldDelimiter", plugin.getWriteFieldDelimiter());
        parameterObj.put("writeMode", plugin.getWriteMode());
        parameterObj.put("encoding", plugin.getWriteEncoding());
        parameterObj.put("nullFormat", plugin.getWriteNullFormat());
        parameterObj.put("dateFormat", plugin.getWriteDateFormat());
        writerObj.put("parameter", parameterObj);
        return writerObj;
    }
}
