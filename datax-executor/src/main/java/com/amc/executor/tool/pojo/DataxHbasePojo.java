package com.amc.executor.tool.pojo;


import com.amc.executor.domain.JobDatasource;
import com.amc.executor.dto.Range;

import com.amc.executor.dto.VersionColumn;
import lombok.Data;

import java.util.List;
import java.util.Map;
/**
 * 用于传参，构建json
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Data
public class DataxHbasePojo {

  private List<Map<String,Object>> columns;

  /**
   * 数据源信息
   */
  private JobDatasource jdbcDatasource;


  private String readerHbaseConfig;

  private String readerTable;

  private String readerMode;

  private String readerMaxVersion;

  private Range readerRange;

  private String writerHbaseConfig;

  private String writerTable;

  private String writerMode;

  private VersionColumn writerVersionColumn;

  private String writerRowkeyColumn;
}
