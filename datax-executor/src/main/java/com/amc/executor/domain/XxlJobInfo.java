package com.amc.executor.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * xxl-job info
 *
 * @author ZhangXX
 */
@Data
@ApiModel
@TableName("xxl_job_info")
public class XxlJobInfo {

	@TableField(value = "id")
	@ApiModelProperty(value = "调度任务id")
	private int id;				// 主键ID

	@TableField(value = "job_group")
	@ApiModelProperty(value = "执行器主键ID")
	private int jobGroup;
	@TableField(value = "job_desc")
	@ApiModelProperty(value = "调度任务")
	private String jobDesc;
}
