package com.amc.executor.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

import lombok.Data;

@ApiModel(value = "datx任务配置")
@Data
@TableName(value = "datax_job_config")
public class DataxJobConfig  {

    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty(value = "主键 datx任务配置id")
    private Long id;

    @TableField(value = "job_info_id")
    @ApiModelProperty(value = "调度任务id")
    private Integer jobInfoId;

    @TableField(exist = false)
    @ApiModelProperty(value = "调度任务描述")
    private String jobDesc;

    @ApiModelProperty("datax运行json")
    @TableField(value = "job_json")
    private String jobJson;

    @ApiModelProperty("jvm参数")
    @TableField(value = "jvm_param")
    private String jvmParam;

    @ApiModelProperty("脚本动态参数")
    @TableField(value = "replace_param")
    private String replaceParam;

    @ApiModelProperty("增量日期格式")
    @TableField(value = "replace_param_type")
    private String replaceParamType;

    @ApiModelProperty("增量方式")
    @TableField(value = "increment_type")
    private String incrementType;

    @ApiModelProperty("增量初始id")
    @TableField(value = "inc_start_id")
    private Long incStartId;



    @TableField(value = "reader_table")
    @ApiModelProperty("datax的读表")
    private String readerTable;

    @ApiModelProperty("主键字段")
    @TableField(value = "primary_key")
    private String primaryKey;

    @ApiModelProperty("增量初始时间")
    @TableField(value = "inc_start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date incStartTime;

    @ApiModelProperty("分区信息")
    @TableField(value = "partition_info")
    private String partitionInfo;

    @TableField(value = "datasource_id")
    @JsonSerialize(using = ToStringSerializer.class)
    @ApiModelProperty("数据源id")
    private Long datasourceId;

    @TableField(exist = false)
    @ApiModelProperty("增量结束id")
    private Long incEndId;
    @TableField(exist = false)
    @ApiModelProperty("增量结束时间")
    private Date incEndTime;
}