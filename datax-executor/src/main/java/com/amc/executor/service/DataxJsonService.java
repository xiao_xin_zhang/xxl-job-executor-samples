package com.amc.executor.service;


import com.amc.executor.dto.DataXJsonBuildDto;

/**
 * datax json构建服务层接口
 *
 * @author ZhangXX
 * 2022/05/14
 */
public interface DataxJsonService {

    /**
     * build datax json
     *
     * @param dto
     * @return
     */
    String buildJobJson(DataXJsonBuildDto dto);
}
