package com.amc.executor.service.impl;

import com.amc.executor.domain.XxlJobInfo;
import com.amc.executor.mapper.XxlJobInfoMapper;
import com.amc.executor.service.XxlJobInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class XxlJobInfoServiceImpl extends ServiceImpl<XxlJobInfoMapper, XxlJobInfo> implements XxlJobInfoService {
    @Override
    public List<XxlJobInfo> selectDataxJobList(){
        return baseMapper.selectDataxJobList();
    }

    @Override
    public List<XxlJobInfo> selectNotBindDataxJobList(Long id){
        return baseMapper.selectNotBindDataxJobList(id);
    }

}
