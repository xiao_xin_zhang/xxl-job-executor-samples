package com.amc.executor.service.impl;

import com.amc.executor.domain.DataxJobConfig;
import com.amc.executor.mapper.DataxJobConfigMapper;
import com.amc.executor.service.DataxJobConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DataxJobConfigServiceImpl extends ServiceImpl<DataxJobConfigMapper, DataxJobConfig> implements DataxJobConfigService {


    @Override
    public DataxJobConfig selectDataxJobConfigById(Long id) {
        return baseMapper.selectDataxJobConfigById(id);
    }

    @Override
    public List<DataxJobConfig> selectDataxJobConfigList(DataxJobConfig dataxJobConfig) {
        return baseMapper.selectDataxJobConfigList(dataxJobConfig);
    }
}
