package com.amc.executor.service.impl;


import com.amc.executor.constant.JdbcConstants;
import com.amc.executor.domain.JobDatasource;
import com.amc.executor.mapper.JobDatasourceMapper;
import com.amc.executor.service.JobDatasourceService;
import com.amc.executor.tool.query.*;
import com.amc.executor.util.AESUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

/**
 * @author ZhangXX
 * 2022/05/14
 */
@Service
@Transactional(readOnly = true)
public class JobDatasourceServiceImpl extends ServiceImpl<JobDatasourceMapper, JobDatasource> implements JobDatasourceService {

    @Resource
    private JobDatasourceMapper jobDatasourceMapper;

    @Override
    public Boolean dataSourceTest(JobDatasource jobDatasource) throws IOException {
        if (JdbcConstants.HBASE.equals(jobDatasource.getDatasource())) {
            return new HBaseQueryTool(jobDatasource).dataSourceTest();
        }
        String userName = AESUtil.decrypt(jobDatasource.getUsername());
        //  判断账密是否为密文
        if (userName == null) {
            jobDatasource.setUsername(AESUtil.encrypt(jobDatasource.getUsername()));
        }
        String pwd = AESUtil.decrypt(jobDatasource.getPassword());
        if (pwd == null) {
            jobDatasource.setPassword(AESUtil.encrypt(jobDatasource.getPassword()));
        }
        if (JdbcConstants.MONGODB.equals(jobDatasource.getDatasource())) {
            return new MongoDBQueryTool(jobDatasource).dataSourceTest(jobDatasource.getDatabaseName());
        }
        if (JdbcConstants.SFTP.equals(jobDatasource.getDatasource())) {
            return new SFTPQueryTool(jobDatasource).dataSourceTest();
        }
        BaseQueryTool queryTool = QueryToolFactory.getByDbType(jobDatasource);
        return queryTool.dataSourceTest();
    }

    @Override
    public List<JobDatasource> selectAllDatasource() {
        return jobDatasourceMapper.selectList(null);
    }

    @Override
    public List<JobDatasource> selectJobDatasourceList(JobDatasource jobDatasource) {
        return jobDatasourceMapper.selectJobDatasourceList(jobDatasource);
    }

}