package com.amc.executor.service.command;

import com.amc.executor.constant.Constants;
import com.amc.executor.domain.DataxJobConfig;
import com.amc.executor.domain.JobDatasource;
import com.amc.executor.enums.IncrementTypeEnum;
import com.amc.executor.mapper.JobDatasourceMapper;
import com.amc.executor.tool.query.BaseQueryTool;
import com.amc.executor.tool.query.QueryToolFactory;
import com.amc.executor.util.SystemUtils;
import com.xxl.job.core.biz.model.TriggerParam;
import com.xxl.job.core.context.XxlJobHelper;

import com.xxl.job.core.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.amc.executor.constant.Constants.SPLIT_COMMA;
import static com.amc.executor.constant.DataXConstant.*;

/**
 * DataX command build
 *
 * @author ZhangXX
 * 2022/05/14
 */

public class BuildCommand {

    /**
     * DataX command build
     *
     * @param dataxJobConfig
     * @param tmpFilePath
     * @param dataXPyPath
     * @return
     */
    public static String[] buildDataXExecutorCmd(DataxJobConfig dataxJobConfig, String tmpFilePath, String dataXPyPath) {
        // command process
        //"--loglevel=debug"
        List<String> cmdArr = new ArrayList<>();
        cmdArr.add("python");
        String dataXHomePath = SystemUtils.getDataXHomePath();
        if (StringUtils.isNotEmpty(dataXHomePath)) {
            dataXPyPath = dataXHomePath.contains("bin") ? dataXHomePath + DEFAULT_DATAX_PY : dataXHomePath + "bin" + File.separator + DEFAULT_DATAX_PY;
        }
        cmdArr.add(dataXPyPath);
        String doc = buildDataXParam(dataxJobConfig);
        if (StringUtils.isNotBlank(doc)) {
            cmdArr.add(doc.replaceAll(SPLIT_SPACE, TRANSFORM_SPLIT_SPACE));
        }
        cmdArr.add(tmpFilePath);
        return cmdArr.toArray(new String[cmdArr.size()]);
    }

    private static String buildDataXParam(DataxJobConfig dataxJobConfig) {
        StringBuilder doc = new StringBuilder();
        String jvmParam = StringUtils.isNotBlank(dataxJobConfig.getJvmParam()) ? dataxJobConfig.getJvmParam().trim() : dataxJobConfig.getJvmParam();
        String partitionStr = dataxJobConfig.getPartitionInfo();
        if (StringUtils.isNotBlank(jvmParam)) {
            doc.append(JVM_CM).append(TRANSFORM_QUOTES).append(jvmParam).append(TRANSFORM_QUOTES);
        }

        String incrementType = dataxJobConfig.getIncrementType();
        String replaceParam = StringUtils.isNotBlank(dataxJobConfig.getReplaceParam()) ? dataxJobConfig.getReplaceParam().trim() : null;

        if (incrementType != null && replaceParam != null) {

            if (IncrementTypeEnum.TIME.getCode().equals(incrementType)) {
                if (doc.length() > 0) doc.append(SPLIT_SPACE);
                String replaceParamType = dataxJobConfig.getReplaceParamType();

                if (StringUtils.isBlank(replaceParamType) || replaceParamType.equals("Timestamp")) {
                    long startTime = dataxJobConfig.getIncStartTime().getTime() / 1000;
                    long endTime = dataxJobConfig.getIncEndTime().getTime() / 1000;
                    doc.append(PARAMS_CM).append(TRANSFORM_QUOTES).append(String.format(replaceParam, startTime, endTime));
                } else {
                    SimpleDateFormat sdf = new SimpleDateFormat(replaceParamType);
                    String startTime = sdf.format(dataxJobConfig.getIncStartTime()).replaceAll(SPLIT_SPACE, PERCENT);
                    String endTime = sdf.format(dataxJobConfig.getIncEndTime()).replaceAll(SPLIT_SPACE, PERCENT);
                    doc.append(PARAMS_CM).append(TRANSFORM_QUOTES).append(String.format(replaceParam, startTime, endTime));
                }
                //buildPartitionCM(doc, partitionStr);
                doc.append(TRANSFORM_QUOTES);

            } else if (IncrementTypeEnum.ID.getCode().equals(incrementType)) {
                long startId = dataxJobConfig.getIncStartId();
                long endId = dataxJobConfig.getIncEndId();
                if (doc.length() > 0) doc.append(SPLIT_SPACE);
                doc.append(PARAMS_CM).append(TRANSFORM_QUOTES).append(String.format(replaceParam, startId, endId));
                doc.append(TRANSFORM_QUOTES);
            }
        }

        if (incrementType != null && IncrementTypeEnum.PARTITION.getCode().equals(incrementType)) {
            if (StringUtils.isNotBlank(partitionStr)) {
                List<String> partitionInfo = Arrays.asList(partitionStr.split(SPLIT_COMMA));
                if (doc.length() > 0) doc.append(SPLIT_SPACE);
                doc.append(PARAMS_CM).append(TRANSFORM_QUOTES).append(String.format(PARAMS_CM_V_PT, buildPartition(partitionInfo))).append(TRANSFORM_QUOTES);
            }
        }

        XxlJobHelper.log("------------------Command parameters:" + doc);
        return doc.toString();
    }


    private void buildPartitionCM(StringBuilder doc, String partitionStr) {
        if (StringUtils.isNotBlank(partitionStr)) {
            doc.append(SPLIT_SPACE);
            List<String> partitionInfo = Arrays.asList(partitionStr.split(SPLIT_COMMA));
            doc.append(String.format(PARAMS_CM_V_PT, buildPartition(partitionInfo)));
        }
    }

    private static String buildPartition(List<String> partitionInfo) {
        String field = partitionInfo.get(0);
        int timeOffset = Integer.parseInt(partitionInfo.get(1));
        String timeFormat = partitionInfo.get(2);
        String partitionTime = DateUtil.format(DateUtil.addDays(new Date(), timeOffset), timeFormat);
        return field + Constants.EQUAL + partitionTime;
    }

}
