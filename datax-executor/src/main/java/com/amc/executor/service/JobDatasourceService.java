package com.amc.executor.service;

import com.amc.executor.domain.JobDatasource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.io.IOException;
import java.util.List;

/**
 * jdbc数据源配置表服务接口
 *
 * @author ZhangXX
 * 2022/05/14
 */
public interface JobDatasourceService extends IService<JobDatasource> {
    /**
     * 测试数据源
     * @param jdbcDatasource
     * @return
     */
    Boolean dataSourceTest(JobDatasource jdbcDatasource) throws IOException;

    /**
     * 获取所有数据源
     * @return
     */
    List<JobDatasource> selectAllDatasource();
    /**
     * 获取所有数据源
     * @return
     */
    List<JobDatasource> selectJobDatasourceList(JobDatasource jobDatasource);
}