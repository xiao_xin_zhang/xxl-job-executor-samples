package com.amc.executor.service;

import com.amc.executor.domain.DataxJobConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface DataxJobConfigService extends IService<DataxJobConfig>{

    DataxJobConfig selectDataxJobConfigById(Long id);

    List<DataxJobConfig> selectDataxJobConfigList(DataxJobConfig dataxJobConfig);
}
