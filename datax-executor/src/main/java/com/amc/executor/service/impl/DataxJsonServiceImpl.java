package com.amc.executor.service.impl;

import com.alibaba.fastjson.JSON;

import com.amc.executor.domain.JobDatasource;
import com.amc.executor.dto.DataXJsonBuildDto;
import com.amc.executor.service.DataxJsonService;
import com.amc.executor.service.JobDatasourceService;
import com.amc.executor.tool.datax.DataxJsonHelper;
import com.amc.executor.util.JSONUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * datax json构建实现类
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Service
public class DataxJsonServiceImpl implements DataxJsonService {

    @Autowired
    private JobDatasourceService jobJdbcDatasourceService;

    @Override
    public String buildJobJson(DataXJsonBuildDto dataXJsonBuildDto) {
        DataxJsonHelper dataxJsonHelper = new DataxJsonHelper();
        // reader
        JobDatasource readerDatasource = jobJdbcDatasourceService.getById(dataXJsonBuildDto.getReaderDatasourceId());
        // reader plugin init
        dataxJsonHelper.initReader(dataXJsonBuildDto, readerDatasource);
        JobDatasource writerDatasource = jobJdbcDatasourceService.getById(dataXJsonBuildDto.getWriterDatasourceId());
        dataxJsonHelper.initWriter(dataXJsonBuildDto, writerDatasource);

        return JSONUtils.formatJson(JSON.toJSONString(dataxJsonHelper.buildJob()));
    }
}