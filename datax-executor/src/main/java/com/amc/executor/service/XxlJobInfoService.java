package com.amc.executor.service;

import com.amc.executor.domain.XxlJobInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

public interface XxlJobInfoService extends IService<XxlJobInfo> {
    List<XxlJobInfo> selectDataxJobList();
    List<XxlJobInfo> selectNotBindDataxJobList(Long id);

}
