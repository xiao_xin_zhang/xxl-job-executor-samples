package com.amc.executor.mapper;

import com.amc.executor.domain.DataxJobConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;


public interface DataxJobConfigMapper extends BaseMapper<DataxJobConfig> {
    DataxJobConfig selectDataxJobConfigById(Long id);

    List<DataxJobConfig> selectDataxJobConfigList(DataxJobConfig dataxJobConfig);
}