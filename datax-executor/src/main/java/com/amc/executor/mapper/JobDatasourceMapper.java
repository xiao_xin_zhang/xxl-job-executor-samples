package com.amc.executor.mapper;

import com.amc.executor.domain.JobDatasource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * jdbc数据源配置表数据库访问层
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Mapper
public interface JobDatasourceMapper extends BaseMapper<JobDatasource> {
    List<JobDatasource> selectJobDatasourceList(JobDatasource jobDatasource);
}