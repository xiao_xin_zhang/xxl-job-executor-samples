package com.amc.executor.mapper;

import com.amc.executor.domain.XxlJobInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 调度任务信息配置表数据库访问层
 *
 * @author ZhangXX
 * 2022/05/14
 */
@Mapper
public interface XxlJobInfoMapper extends BaseMapper<XxlJobInfo> {
    List<XxlJobInfo> selectDataxJobList();

    /**
     * @return
     */
    List<XxlJobInfo> selectNotBindDataxJobList(@Param(value = "id") Long id);
}