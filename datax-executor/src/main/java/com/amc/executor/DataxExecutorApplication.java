package com.amc.executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.env.Environment;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @author ZhangXX
 *
 * @author ZhangXX
 * 2022/05/14
 */
@SpringBootApplication
public class DataxExecutorApplication {

    private static Logger logger = LoggerFactory.getLogger(DataxExecutorApplication.class);

    public static void main(String[] args) throws UnknownHostException {
        Environment env = new SpringApplication(DataxExecutorApplication.class).run(args).getEnvironment();
        System.out.println("(♥◠‿◠)ﾉﾞ  DataxExecutor系统启动成功   ლ(´ڡ`ლ)ﾞ )");
        String envPort = env.getProperty("server.port");
        String envContext = env.getProperty("server.contextPath");
        String port = envPort == null ? "8080" : envPort;
        String context = envContext == null ? "" : envContext;
        String path = port + "" + context + "/doc.html";
        String externalAPI = InetAddress.getLocalHost().getHostAddress();
        logger.info(
                "Access URLs:\n----------------------------------------------------------\n\t"
                        + "Local-API: \t\thttp://127.0.0.1:{}\n\t"
                        + "External-API: \thttp://{}:{}\n\t"
                        + "web-URL: \t\thttp://127.0.0.1:{}/index\n----------------------------------------------------------",
                path, externalAPI, path, port);
    }

}