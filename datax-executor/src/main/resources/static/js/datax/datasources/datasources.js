var rdbmsArray = ['mysql', 'oracle', 'postgresql', 'sqlserver', 'clickhouse', 'hive'];

// 增量类型
function selDatasourceType() {
    var datasource = $("#datasource").val();
    var jdbcUrl = $("#jdbcUrl").val();
    getShowStrategy(datasource);
    if (datasource === 'mysql') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:mysql://{host}:{port}/{database}') : jdbcUrl;
        $("#jdbcDriverClass").val('com.mysql.jdbc.Driver');
    } else if (datasource === 'oracle') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:oracle:thin:@//{host}:{port}/{database}') : jdbcUrl;
        $("#jdbcDriverClass").val('oracle.jdbc.OracleDriver');
    } else if (datasource === 'postgresql') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:postgresql://{host}:{port}/{database}') : jdbcUrl;
        $("#jdbcDriverClass").val('org.postgresql.Driver');
    } else if (datasource === 'sqlserver') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:sqlserver://{host}:{port};DatabaseName={database}') : jdbcUrl;
        $("#jdbcDriverClass").val('com.microsoft.sqlserver.jdbc.SQLServerDriver');
    } else if (datasource === 'clickhouse') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:clickhouse://{host}:{port}/{database}') : jdbcUrl;
        $("#jdbcDriverClass").val('ru.yandex.clickhouse.ClickHouseDriver');
    } else if (datasource === 'hive') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('jdbc:hive2://{host}:{port}/{database}') : jdbcUrl;
        $("#jdbcDriverClass").val('org.apache.hive.jdbc.HiveDriver');
    } else if (datasource === 'mongodb') {
        $.common.isEmpty(jdbcUrl) ? $("#jdbcUrl").val('mongodb://[username:password@]host1[:port1][,...hostN[:portN]]][/[database][?options]]') : jdbcUrl;
    }

}

function getShowStrategy(datasource) {
    if (rdbmsArray.includes(datasource)) {
        $(".mongodb").hide();
        $(".hbase").hide();
        $(".sftp").hide();
        $(".rdbms").show();
    } else if (datasource === 'mongodb') {
        $(".rdbms").hide();
        $(".hbase").hide();
        $(".sftp").hide();
        $(".mongodb").show();
    } else if (datasource === 'hbase') {
        $(".rdbms").hide();
        $(".mongodb").hide();
        $(".sftp").hide();
        $(".hbase").show();
    } else if (datasource === 'sftp') {
        $(".rdbms").hide();
        $(".mongodb").hide();
        $(".hbase").hide();
        $(".sftp").show();
    }
}
