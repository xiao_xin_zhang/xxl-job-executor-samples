var formatJsonUrl = ctx + "api/dataxJson/format?strJson=";
// 增量类型
function selIncrementType() {
    var type=$("#incrementType").val();

    if (type=="1") {
        $(".inc-pk").show();
        $(".inc-time").hide();
        $(".inc-time-pk").show();
        $(".inc-partition").hide();
        $("#replaceParamLabel").text("ID增量参数");
        $("#replaceParam").attr("placeholder", "-DstartId='%s' -DendId='%s'");
    }else if (type=="2") {
        $(".inc-pk").hide();
        $(".inc-time").show();
        $(".inc-time-pk").show();
        $(".inc-partition").hide();
        $("#replaceParamLabel").text("时间增量参数");
        $("#replaceParam").attr("placeholder", "-DlastTime='%s' -DcurrentTime='%s'");
    }else if (type=="3") {
        $(".inc-pk").hide();
        $(".inc-time").hide();
        $(".inc-time-pk").hide();
        $(".inc-partition").show();
    }else{
        $(".inc-pk").hide();
        $(".inc-time").hide();
        $(".inc-time-pk").hide();
        $(".inc-partition").hide();
    }
}
// 下载json文件
function downloadJson() {
    // 下载文件方法
    var eleLink = document.createElement('a');
    eleLink.download = "person.json"; // 文件名
    eleLink.style.display = 'none';
    // 字符内容转变成blob地址
    var blob = new Blob([$('#json-renderer').text() + "\r\n"]);
    eleLink.href = URL.createObjectURL(blob);
    // 触发点击
    document.body.appendChild(eleLink);
    eleLink.click();
    // 然后移除
    document.body.removeChild(eleLink);
}

/**
 * 插件格式化json
 */
function renderJson() {
    try {
        var input = eval('(' + $('#json-input').val() + ')');
    }
    catch (error) {
        return $.modal.alertError("不能够解析 JSON: " + error);
    }
    var options = {
        collapsed: $('#collapsed').is(':checked'),
        rootCollapsable: $('#root-collapsable').is(':checked'),
        withQuotes: $('#with-quotes').is(':checked'),
        withLinks: $('#with-links').is(':checked')
    };
    $('#json-renderer').jsonViewer(input, options);
}

/**
 * 后台格式化Json
 * @param strJson
 */
function formatJson(strJson) {
    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: formatJsonUrl,
        data: strJson,
        type: 'POST',
        beforeSend: function () {
            $.modal.loading("正在处理中，请稍候...");
        },
        success: function(result) {
            if (result.code == web_status.SUCCESS) {
                console.log(result.data);
                $('#json-input').val(result.data);
                $.modal.closeLoading();
            } else {
                $.modal.msgError(result.msg);
                $.modal.closeLoading();
            }
        }
    });
}