var prefix = ctx + "api/metadata";
var buildJsonUrl = ctx + "api/dataxJson/buildJson";
var addDataxConfUrl = ctx + "datax/config/add";
var prefix1 = ctx;

var schemaArr = ['postgresql']
var rdbmsArray = ['mysql', 'oracle', 'postgresql', 'sqlserver', 'clickhouse', 'hive'];
var sftpArray = ['sftp'];
var hbaseArray = ['hbase'];
var mongodbArray = ['mongodb'];

// 初始化绑定事件
$(document).ready(function () {
    // 读数据源改变事件
    $("#readerDatasourceId").on('change', function () {
        var datasourceId = $("#readerDatasourceId").val();
        //获取Select选择的Text(数据源类型—数据源名称)
        var checkText = $("#readerDatasourceId").find("option:selected").text();
        temp = checkText.split('-');
        var readerPlugin = pluginShowStrategy(temp[0], "Reader");
        pluginRefresh("reader", readerPlugin);
        // 数据源是否有schema
        if (!schemaArr.indexOf(temp[1]) == -1) {
            $('#readerSchemaDiv').show();
            getDBSchema(datasourceId, "readerSchemas");
        } else {
            $('#readerSchemaDiv').hide();
            //var datas = [[${@dict.getLabel('datasource_type',temp[0])}]];
            if (rdbmsArray.includes(temp[0])) {
                getTablesByIdAndSchema("readerTables", "readerSchemas", datasourceId);
            }
        }
    });
    // 读tableSchema改变事件
    $(document).on('change', "#readerTableSchema", function () {
        var datasourceId = $("#readerDatasourceId").val();
        var tableSchema = $("#readerSchemas").val();
        getTablesByIdAndSchema("readerTables", "readerSchemas", datasourceId, tableSchema);
    });
    // 读table改变事件
    $(document).on('change', "#readerTables", function () {
        var datasourceId = $("#readerDatasourceId").val();
        var tableName = $("#readerTables").val();
        getColumns(datasourceId, tableName, "readerColumns");
    });

    // 写数据源改变事件
    $("#writerDatasourceId").on('change', function () {
        var datasourceId = $("#writerDatasourceId").val();
        //获取Select选择的Text
        var checkText = $("#writerDatasourceId").find("option:selected").text();
        console.log(checkText);
        temp = checkText.split('-');
        var writerPlugin = pluginShowStrategy(temp[0], "Writer");
        pluginRefresh("writer", writerPlugin);
        // 数据源是否有schema
        if (!schemaArr.indexOf(temp[0]) == -1) {
            $('#writerSchemaDiv').show();
            getDBSchema(datasourceId, "readerSchemas");
        } else {
            $('#writerSchemaDiv').hide();
            if (rdbmsArray.includes(temp[0])) {
                getTablesByIdAndSchema("writerTables", "writerSchemas", datasourceId);
            }
        }
    });
    // 写tableSchema改变事件
    $(document).on('change', "#writerTableSchema", function () {
        var datasourceId = $("#writerDatasourceId").val();
        var tableSchema = $("#writerSchemas").val();
        getTablesByIdAndSchema("writerTables", "writerSchemas", datasourceId, tableSchema);
    });
    // 写table改变事件
    $(document).on('change', "#writerTables", function () {
        var datasourceId = $("#writerDatasourceId").val();
        var tableName = $("#writerTables").val();
        getColumns(datasourceId, tableName, "writerColumns");
    });

});

// 获取当前数据源下的Schema
function getDBSchema(datasourceId, schemaId) {
    $.get(prefix + "/getDBSchema?datasourceId=" + datasourceId, function (result) {
        if (result.code == web_status.SUCCESS) {
            //创建Schema的下拉选项
            createSchemaOption(result.data, schemaId);
        } else {
            $.modal.msgError(result.msg);
        }
    });
}

// 获取当前数据源下的表
function getTablesByIdAndSchema(tablesId, schemaId, datasourceId, tableSchema) {
    var uri = prefix + "/getTables?datasourceId=" + datasourceId;
    if (!$.common.isEmpty(tableSchema)) {
        uri += "&tableSchema=" + tableSchema;
    }
    $.get(uri, function (result) {
        if (result.code == web_status.SUCCESS) {
            if (!$.common.isEmpty(tableSchema)) {
                //创建Schema的下拉选项
                createSchemaOption(result.data, schemaId);
            } else {
                //创建表的下拉选项
                createTableOption(result.data, tablesId);
            }
        } else {
            $.modal.msgError(result.msg);
        }
    });
}

/**
 * 获取当前表下的所有列
 * @param datasourceId 数据源id
 * @param tableName 表名称
 * @param columnsName table多选框的名称
 */

function getColumns(datasourceId, tableName, columnsName) {
    $.get(prefix + "/getColumns?datasourceId=" + datasourceId + "&tableName=" + tableName, function (result) {
        if (result.code == web_status.SUCCESS) {
            var arr = result.data;
            createCheckBox(arr, columnsName);
            //createCheckBox2(arr, columnsName);
            $(".sortable-list").sortable().disableSelection();
        } else {
            $.modal.msgError(result.msg);
        }
    });
}

/**
 * 根据数据源id和sql语句获取所有字段
 */
function getColumnsByQuerySql() {
    var datasourceId = $("#readerDatasourceId").val();
    var querySql = $("#querySql").val();
    $.get(prefix + "/getColumnsByQuerySql?datasourceId=" + datasourceId + "&querySql=" + querySql, function (result) {
        if (result.code == web_status.SUCCESS) {
            var arr = result.data;
            createCheckBox(arr, "readerColumns");
        } else {
            $.modal.msgError(result.msg);
        }
    });
}


/**
 * 动态创建表的下拉选项
 * @param arr
 * @param tablesId readerTablesId  writerTablesId
 */
function createTableOption(arr, tablesId) {
    $("#" + tablesId).empty();
    $("#" + tablesId).append("<option selected value=''>---请选择---</option>");
    for (var i = 0; i < arr.length; i++) {
        //添加option元素
        $("#" + tablesId).append("<option value='" + arr[i] + "'>" + arr[i] + "</option>");
    }
}

/**
 * 动态创建Schema的下拉选项
 * @param arr
 * @param tablesId readerSchemaId  writerSchemaId
 */
function createSchemaOption(arr, schemaId) {
    $("#" + schemaId).empty();
    $("#" + schemaId).append("<option selected value=''>---请选择---</option>");
    for (var i = 0; i < arr.length; i++) {
        //添加option元素
        $("#" + schemaId).append("<option value='" + arr[i] + "'>" + arr[i] + "</option>");
    }
}


// 动态创建多选框
function createCheckBox(arr, name) {
    var columnsDiv = document.getElementById(name);
    columnsDiv.innerHTML = "";
    var ul = document.createElement("ul");
    for (var i = 0; i < arr.length; i++) {
        // 加入复选框
        var checkBox = document.createElement("input");
        checkBox.setAttribute("type", "checkbox");
        checkBox.setAttribute("name", name);
        checkBox.setAttribute("id", arr[i]);
        var li = document.createElement("li");
        li.appendChild(checkBox);
        li.appendChild(document.createTextNode(arr[i]));
        ul.appendChild(li);
    }
    columnsDiv.appendChild(ul);
}

// 动态创建多选框
function createCheckBox2(arr, name) {
    var columnsDiv = document.getElementById(name);
    columnsDiv.innerHTML = "";
    var ul = document.createElement("ul");
    ul.setAttribute("class", "sortable-list connectList agile-list");
    for (var i = 0; i < arr.length; i++) {
        // 加入复选框
        var checkBox = document.createElement("input");
        checkBox.setAttribute("type", "checkbox");
        checkBox.setAttribute("name", name);
        checkBox.setAttribute("id", arr[i]);
        var li = document.createElement("li");
        li.setAttribute("class", "success-element");
        li.appendChild(checkBox);
        li.appendChild(document.createTextNode(arr[i]));
        ul.appendChild(li);
    }
    columnsDiv.appendChild(ul);
}

// 全选  反选  取消全选
function opcheckboxed(objName, type) {
    var objNameList = document.getElementsByName(objName);
    if (null != objNameList) {
        for (var i = 0; i < objNameList.length; i++) {
            if (objNameList[i].checked == true) {
                if (type != 'checkall') {  // 非全选
                    objNameList[i].checked = false;
                }
            } else {
                if (type != 'uncheckall') {  // 非取消全选
                    objNameList[i].checked = true;
                }
            }
        }
    }
}

// 获取选中的
function getAllcheckboxed(columnsId, fieldId) {
    var objNameList = document.getElementsByName(columnsId);
    if (null != objNameList) {
        var fieldDiv = document.getElementById(fieldId);
        fieldDiv.innerHTML = "";
        var ul = document.createElement("ul");
        ul.setAttribute("class", "sortable-list connectList agile-list");
        for (var i = 0; i < objNameList.length; i++) {
            if (objNameList[i].checked == true) {
                console.log(objNameList[i]);
                var li = document.createElement("li");
                li.setAttribute("class", "success-element");
                li.appendChild(document.createTextNode(objNameList[i].id));
                ul.appendChild(li);
            }
        }
        fieldDiv.appendChild(ul);
    }
}

/**
 * 根据数据源id和sql语句获取所有字段
 */
function buildJson() {
    var readerDatasourceId = $("#readerDatasourceId").val();
    var readerTables = [];
    readerTables.push($("#readerTables").val());
    var readerColumns = getAllLiValue('readerField ul li');
    var writerDatasourceId = $("#writerDatasourceId").val();

    var writerTables = [];
    writerTables.push($("#writerTables").val());
    var writerColumns = getAllLiValue('writerField ul li');
    var readerSplitPk = $("#readerSplitPk").val();
    var whereParams = $("#whereParams").val();
    var querySql = $("#querySql").val();
    var preSql = $("#preSql").val();
    var postSql = $("#postSql").val();

    //
    var rdbmsReader = {
        readerSplitPk: readerSplitPk,
        whereParams: whereParams,
        querySql: querySql
    };
    //
    var rdbmsWriter = {
        preSql: preSql,
        postSql: postSql
    };
    var ftpReader = {
        readerProtocol: $("#readerProtocol").val(),
        readerPath: $("#readerPath").val(),
        readerEncoding: $("#readerEncoding").val(),
        readerNullFormat: $("#readerNullFormat").val(),
        readerColumn: $("#readerColumn").val()
    };
    var ftpWriter = {
        writerProtocol: $("#writerProtocol").val(),
        writerPath: $("#writerPath").val(),
        writerFileName: $("#writerFileName").val(),
        writeMode: $("#writeMode").val(),
        writerEncoding: $("#writerEncoding").val(),
        writerNullFormat: $("#writerNullFormat").val(),
        writerDateFormat: $("#writerDateFormat").val(),
        writerFileFormat: $("#writerFileFormat").val(),
        writerSuffix: $("#writerSuffix").val(),
    };

    var data = {
        readerDatasourceId: readerDatasourceId,
        readerTables: readerTables,
        readerColumns: readerColumns,
        writerDatasourceId: writerDatasourceId,
        writerTables: writerTables,
        writerColumns: writerColumns,
        rdbmsReader: rdbmsReader,
        rdbmsWriter: rdbmsWriter,
        ftpReader: ftpReader,
        ftpWriter: ftpWriter
    };

    $.ajax({
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        url: buildJsonUrl,
        data: JSON.stringify(data),
        type: 'POST',
        beforeSend: function () {
            $.modal.loading("正在处理中，请稍候...");
        },
        success: function (result) {
            if (result.code == web_status.SUCCESS) {
                $('#json-input').val(result.data);
                renderJson();
                $.modal.closeLoading();
            } else {
                $.modal.msgError(result.msg);
                $.modal.closeLoading();
            }

        }
    });

}


/**
 * 获取li的值
 * @param id
 * @returns {[]}
 */
function getAllLiValue(id) {
    var arr = [];
    $('#' + id).each(function () {
        var liVal = $(this).text();
        arr.push(liVal);
    });
    return arr;
}

/**
 * Datax配置任务绑定调度任务
 */
function bindTask() {
    var jobJson = $('#json-input').val();
    var title = '绑定调度任务';
    var optoins = {
        id: title,
        title: [title],
        anim: 5,
        url: addDataxConfUrl,
        success: function (layero, index) {
            var body = $.modal.getChildFrame(index);
            console.log(body.contents().find("#json-input"));
            body.contents().find("#json-input").val(jobJson);
        },
        callBack: function (index, layero) {
            var iframeWin = layero.find('iframe')[0];
            iframeWin.contentWindow.submitHandler2(index, layero);
        }
    }
    $.modal.openOptions(optoins);
}

/**
 * 切换插件
 */
/**
 *
 * @param pluginType reader或者writer
 * @param plugin RDBMS mongodb hbase sftp
 */
function pluginRefresh(pluginType, plugin) {
    $.ajax({
        type: "post",
        url: ctx + "datax/" + pluginType + "/localrefresh",
        async: false,
        data: {
            "fragment": plugin
        },
        success: function (data) {
            if (pluginType === "reader") {
                $("#readerPlugin").html(data);
            }
            if (pluginType === "writer") {
                $("#writerPlugin").html(data);
            }
        }
    });
}

/**
 *
 * @param datasourcesType
 */
function pluginShowStrategy(datasourcesType, pluginType) {
    console.log(datasourcesType);
    if (rdbmsArray.includes(datasourcesType)) {
        return "RDBMS" + pluginType;
    } else if (hbaseArray.includes(datasourcesType)) {
        return "Hbase" + pluginType;
    } else if (mongodbArray.includes(datasourcesType)) {
        return "Mongodb" + pluginType;
    } else if (sftpArray.includes(datasourcesType)) {
        return "SFTP" + pluginType;
    }
}

//Long readerDatasourceId;

//List<String> readerTables;

//List<String> readerColumns;

//Long writerDatasourceId;

//List<String> writerTables;

//List<String> writerColumns;

//HiveReaderDto hiveReader;

//HiveWriterDto hiveWriter;

//HbaseReaderDto hbaseReader;

//HbaseWriterDto hbaseWriter;

//RdbmsReaderDto rdbmsReader;

//RdbmsWriterDto rdbmsWriter;

//MongoDBReaderDto mongoDBReader;

//MongoDBWriterDto mongoDBWriter;