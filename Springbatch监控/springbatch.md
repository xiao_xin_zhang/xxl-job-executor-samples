org\springframework\batch\spring-batch-core\3.0.6.RELEASE\spring-batch-core-3.0.6.RELEASE.jar目录下,并且针对不同的数据库提供了不同的数据库脚本

![Center](https://image.dandelioncloud.cn/images/20220731/1c6c248110694cc0999d4771bbf1964f.png)

SpringBatch框架进行元数据的管理共有九张表:

![Center 1](https://image.dandelioncloud.cn/images/20220731/fceb4ce445f64a86af5ac8ba59d3ee3d.png)

下面分别对9张表的数据结构讲解哈:

**BATCH_JOB_INSTANCE(作业实例表,存放JOB的实例信息)**

| 字段名称        | 字段 类型 | 字段说明                                                     |
| --------------- | --------- | ------------------------------------------------------------ |
| JOB_INSTANCE_ID | bigint    | 主键 ，作业实例编号,根据BATCH_JOB_SEQ自动生成                |
| VERSION         | bigint    | 版本号                                                       |
| JOB_NAME        | varchar   | 作业名称 即在配置文件定义的JobId字段内容                     |
| JOB_KEY         | varchar   | 作业标识,根据作业参数序列化生成的标识,需要注意通过JOB_NAME+JOB_KEY能够唯一区分一个作业实例 |

注意:JOB_KEY———如果同一个Job,则JOB_KEY一定不能相同,籍作业参数不能相同,如果不是同一个Job,JOB_KEY可以相同,也就是作业参数可以相同

**BATCH_JOB_EXECUTION_PARAMS(作业参数表,用于存放每个JOB执行的时候的参数信息,即对于的JOB实例)**

| 字段名称         | 字段类型 | 字段说明                                                  |
| ---------------- | -------- | --------------------------------------------------------- |
| JOB_EXECUTION_ID | bigint   | 外键ID,作业执行器ID编号，一个作业实例可能又会多行参数记录 |
| TYPE_CD          | varchar  | 参数的类型，总共有四种 String、Date、Double、Long         |
| KEY_NAME         | varchar  | 参数的名字                                                |
| STRING_VAL       | varchar  | 参数如果是String,就存放String类型的参数值                 |
| DATE_VAL         | datetime | 参数如果是Date,就存放Date类型的参数值                     |
| LONG_VAL         | bigint   | 参数如果是Long,就存放Long类型的参数值                     |
| DOUBLE_VAL       | double   | 参数如果是Double,就存放Double类型的参数值                 |
| IDENTIFYING      | char     | 用于标识作业参数是否标识作业实例                          |

**BATCH_JOB_EXECUTION(作业执行器表,存放当前作业的执行信息,创建时间、执行时间、执行结束时间、执行状态、执行Job实例等信息)**

| 字段名称                   | 字段类型 | 字段说明                                                   |
| -------------------------- | -------- | ---------------------------------------------------------- |
| JOB_EXECUTION_ID           | bigint   | 主键                                                       |
| VERSION                    | bigint   | 版本号                                                     |
| JOB_INSTANCE_ID            | bigint   | 作业实例ID                                                 |
| CREATE_TIME                | datetime | 作业执行器创建时间                                         |
| START_TIME                 | datetime | 作业执行器开始执行时间                                     |
| END_TIME                   | datetime | 作业执行器结束时间                                         |
| STATUS                     | varchar  | 作业执行器执行的状态:COMPLETED、FAILED、STARTING、UNKNOW等 |
| EXIT_CODE                  | varchar  | 作业执行器退出代码 如：UNKNOW、EXECUTING、COMPLETED等      |
| EXIT_MESSAGE               | varchar  | 作业执行器退出信息,通常存放异常信息                        |
| LAST_UPDATED               | datetime | 本条记录上次更新时间                                       |
| JOB_CONFIGURATION_LOCATION | varchar  | 作业配置文件的位置                                         |
|                            |          |                                                            |

运用BATCH_JOB_INSTANCE和BATCH_JOB_EXECUTION两张表联合查询Job信息

```
SELECT
	bi.JOB_NAME,
	bi.JOB_KEY,
	be.CREATE_TIME,
	be.START_TIME,
	be.END_TIME,
	be.STATUS 
FROM
	batch_job_instance bi
	LEFT JOIN batch_job_execution be ON bi.JOB_INSTANCE_ID = be.JOB_INSTANCE_ID 
WHERE
	bi.JOB_NAME = 'billJob'
```

**BATCH_JOB_EXECUTION_CONTENXT(作业执行上下文表,存放上下文信息)**

| 字段名称           | 字段类型 | 字段说明                   |
| ------------------ | -------- | -------------------------- |
| JOB_EXECUTION_ID   | bigint   | 外键ID,作业执行器编号      |
| SHORT_CONTEXT      | varchar  | 作业执行器上下文字符串格式 |
| SERIALIZED_CONTEXT | text     | 序列化的作业执行上下文     |

**BATCH_JOB_EXECUTION_SEQ表（用于BATCH_JOB_EXECUTION和BATCH_JOB_EXECUTION_CONTEXT提供主键生成）**

**BATCH_JOB_SEQ表（用于BATCH_JOB_INSTNCE和BATCH_JOB_EXECUTION_PARAMS提供主键生成）**

**BATCH_STEP_EXECUTION(作业步执行器，存放每个Step执行器的信息：开始时间、执行完成时间、读/写次数等)**

| 字段名称           | 字段类型 | 字段说明                                                     |
| ------------------ | -------- | ------------------------------------------------------------ |
| STEP_EXECUTION_ID  | bigint   | 主键                                                         |
| VERSION            | bigint   | 版本                                                         |
| STEP_NAME          | varchar  | 作业步的名称                                                 |
| JOB_EXECUTION_ID   | bigint   | 作业执行器ID                                                 |
| START_TIME         | datetime | 作业步执行器执行开始时间                                     |
| END_TIME           | datetime | 作业步执行器执行结束时间                                     |
| STATUS             | varchar  | 作业步执行器执行状态  COMPLETED, STARTING, STARTED, STOPPING, STOPPED, FAILED, ABANDONED, UNKNOWN  存在于org.springframework.batch.core.BatchStatus |
| COMMIT_COUNT       | bigint   | 事务提交次数                                                 |
| READ_COUNT         | bigint   | 读数据次数                                                   |
| FILTER_COUNT       | bigint   | 过滤掉的数据次数                                             |
| WRITE_COUNT        | bigint   | 写数据次数                                                   |
| READ_SKIP_COUNT    | bigint   | 读数据跳过的次数                                             |
| WRITE_SKIP_COUNT   | bigint   | 写数据跳过的次数                                             |
| PROCESS_SKIP_COUNT | bigint   | 处理数据跳过的次数                                           |
| ROLLBACK_COUNT     | bigint   | 事务回滚的次数                                               |
| EXIT_CODE          | varchar  | 作业步执行器退出编码，状态码存在于org.springframework.batch.core.ExitStauts |
| EXIT_MESSAGE       | varchar  | 作业步执行器退出描述,一般是异常信息                          |
| LAST_UPDATED       | datetime | 本条记录上次更新时间                                         |
|                    |          |                                                              |

**BATCH_STEP_EXECUTION_CONTEXT(作业步执行器上下文,存放作业步执行的上下文信息)**

| 字段名称           | 字段类型 | 字段说明                     |
| ------------------ | -------- | ---------------------------- |
| STEP_EXECUTION_ID  | bigint   | 作业步执行器ID               |
| SHORT_CONTEXT      | varchar  | 作业步执行器上下文字符串格式 |
| SERIALIZED_CONTEXT | text     | 序列化作业步执行器上下文     |

**BATCH_STEP_EXECUTION_SEQ（用于BATCH_STEP_EXECUTION和BATCH_STEP_EXECUTION_CONTEXT提供主键）**



**在Spring Batch中，元数据存储在关系型数据库中，如果需要手动删除元数据，可以使用以下SQL语句：**  
```
删除JOB_EXECUTION_PARAMS表中的所有记录：  
DELETE FROM BATCH_JOB_EXECUTION_PARAMS;  
删除STEP_EXECUTION_CONTEXT表中的所有记录：  
DELETE FROM BATCH_STEP_EXECUTION_CONTEXT;  
删除STEP_EXECUTION表中的所有记录：  
DELETE FROM BATCH_STEP_EXECUTION;  
删除STEP_EXECUTION_CONTEXT表中的所有记录：  
DELETE FROM BATCH_JOB_EXECUTION_CONTEXT;  
删除JOB_EXECUTION表中的所有记录：  
DELETE FROM BATCH_JOB_EXECUTION;  
删除JOB_INSTANCE表中的所有记录：  
DELETE FROM BATCH_JOB_INSTANCE;
请注意，在删除元数据之前，请确保备份数据库以防止数据丢失。此外，删除元数据可能会影响其他正在运行的作业和步骤，因此请谨慎操作。  
```

**删除某个实例的所有数据 JOB_INSTANCE_ID为实例id**
```
DELETE FROM BATCH_STEP_EXECUTION_CONTEXT WHERE STEP_EXECUTION_ID IN (
SELECT STEP_EXECUTION_ID FROM BATCH_STEP_EXECUTION WHERE JOB_EXECUTION_ID IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE JOB_INSTANCE_ID = 99));

DELETE FROM BATCH_JOB_EXECUTION_CONTEXT WHERE JOB_EXECUTION_ID IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE JOB_INSTANCE_ID = 99);

DELETE FROM BATCH_STEP_EXECUTION WHERE JOB_EXECUTION_ID IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE JOB_INSTANCE_ID = 99);

DELETE FROM BATCH_JOB_EXECUTION_PARAMS WHERE JOB_EXECUTION_ID IN (SELECT JOB_EXECUTION_ID FROM BATCH_JOB_EXECUTION WHERE JOB_INSTANCE_ID = 99);

DELETE FROM BATCH_JOB_EXECUTION WHERE JOB_INSTANCE_ID=99;

DELETE FROM BATCH_JOB_INSTANCE WHERE JOB_INSTANCE_ID=99;
```
**查询当前正在执行中的任务**
```
SELECT 
I.JOB_NAME,
E.JOB_EXECUTION_ID,
E.START_TIME,
E.END_TIME,
E.STATUS,
E.EXIT_CODE,
E.EXIT_MESSAGE,
E.CREATE_TIME,
E.LAST_UPDATED,
E.VERSION,
E.JOB_INSTANCE_ID,
E.JOB_CONFIGURATION_LOCATION 
FROM
	BATCH_JOB_EXECUTION E,BATCH_JOB_INSTANCE I 
WHERE
	E.JOB_INSTANCE_ID = I.JOB_INSTANCE_ID 
	-- AND I.JOB_NAME =? 
	AND E.END_TIME IS NULL 
	-- AND E.START_TIME >= TRUNC(SYSDATE) NOW()
	AND E.START_TIME >= CURDATE()
ORDER BY
	E.JOB_EXECUTION_ID DESC
```